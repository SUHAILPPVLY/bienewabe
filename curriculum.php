<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Bienewabe | World School | An Initative of The B School International</title>
    <meta name=description content="Bienewabe World School An Initative of The B School International, Kottakkal">
    <meta name="keywords" content="Bienewabe World School An Initative of The B School International, Kottakkal">
    <?php include('links/style.php'); ?>
</head>

<body>
    <!-- header area  -->
    <?php include('links/header.php'); ?>
    <!-- Return to Top -->
    <a href="javascript:" id="return-to-top">
        <img src="asset/icons/top-arrow.png" alt="">
    </a>
    <!-- Return to Top -->

    <!-- contact Banner image  -->
    <section class="bienewabe-curricu">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-left">
                    <div class="bien_titl">
                        <h2>Curriculum</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- contact Banner image  -->
    <section class="padding-50 bienwall-dots">
        <div class="container">
            <div class="col-md-12 col-lg-12 text-center">
                <div class="animate one">
                    <span>C</span><span>u</span><span>r</span><span>r</span><span>i</span><span>c</span><span>u</span><span>l</span><span>u</span><span>m</span>&nbsp;
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 mt-3 mb-2 biene-mtop-mob">
                    <div class="biene-wabe-fit-deno">
                        <div class="biene-head-alfitar text-center">
                            <img src="asset/img/al-fitrah-logo.png" alt="">
                        </div>
                        <div class="biene-child">
                            <h5>Pre-School (3 - 6 Yrs Old)</h5>
                            <p data-aos="fade-up" data-aos-duration="2000">A novel and innovative system of learning for children aged 3yrs, Nour-el-Bayan- designed in Egypt about 8 yrs ago which is gaining recognition in countries like, the UK, US UAE, KSA, Sudan, Srilanka, South Atrica, China, France,
                                Russia etc. The system enables a child to read the Qur'an completely from the beginninng to the end in accordance with the traditions combined with child-friendly narration. It also incorporates thee study of English language,
                                Mathematics, Islamic studies and other character building features. This method of learning helps the child to bloom in tune with his/her interests and creativity rather than following a ready made.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 mt-3 mb-2">
                    <div class="">
                        <img src="asset/img/biene-curriculam-img.png" width="100%" alt="">
                    </div>
                    <!-- <div class="row">
                        <div class="col-lg-6 col-md-6 mt-4 biene-mtop-mob" data-aos="zoom-in" data-aos-duration="2000">
                            <div class="biene-mobile-none biene-desk-ten">
                                <img src="asset/img/biene-currilm-come.01.png" width="100%" alt="">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 mt-4 biene-mtop-mob" data-aos="zoom-in" data-aos-duration="3000">
                            <div class="biene-mobile-none biene-desk-ten">
                                <img src="asset/img/biene-currilm-come.02.png" width="100%" alt="">
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
    </section>
    <section class="beine-padd-20">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-xs-12 biene-mtop-mob mt-4 " data-aos="zoom-in" data-aos-duration="2000">
                    <div class="beien-data-howa">
                        <img src="asset/img/biene-currilm-edu.png" width="100%" alt="">
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-xs-12 biene-mtop-mob mt-4">
                    <div class="biene-data-edu">
                        <h3 data-aos="fade-up" data-aos-duration="1000">IGCSE (International General Certificate of Secondary Education)</h3>
                        <p data-aos="fade-up" data-aos-duration="2000">A globally recognised excellent programme with a wide range of subject options that develop students skills, creative thinking, enquiry and problem-solving. It focuses on the development of well balanced and principled young adults.
                            The curriculum offers opportunities to explore subjects in depth in application level, through a rigorous and internationally recognized programme, in order to prepare them to succeed their higher studies.</p>
                        <p data-aos="fade-up" data-aos-duration="3000">Students are leaders in their own learning, exploring and reflecting on their context within a specific situation and within the world as a whole. The emphasis for learning and discovery is placed on self exploration and underlines
                            personal responsibility, organization and timee management.
                        </p>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="padding-bottom">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-md-9 col-xs-12 biene-mtop-mob mt-4">
                    <div class="biene-data-edu">
                        <h3 data-aos="fade-up" data-aos-duration="2000">Assessment & Grading</h3>
                        <p data-aos="fade-up" data-aos-duration="3000">The assessment practices promote independent learníing by identifying the areas of improvement along with strengths in a manner that is understandable to learners, hence enabling them to retlect upon their progress and set future
                            targets. The academic progress of the learners is systematically monitored by teachers and the supervisors through close observation as well as through formative & summative assessments, assessment of class work, tests, team-work,
                            projects and term examinations. Midyear assessments take place in October and end of the year assessments take place in March each year. Report cards are given to parents at the end of each term.</p>

                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-xs-12 biene-mtop-mob mt-4" data-aos="zoom-in" data-aos-duration="2000">
                    <div class="beien-data-howa">
                        <img src="asset/img/biene-currilm-grade.png" width="100%" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Environment Section  -->
    <section class="beine-bg-envt">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-xs-12 biene-mtop-mob mt-4">
                    <div class="biene-envt-call">
                        <div class="biene-envt-subs">
                            <img src="asset/img/biene-awa-envirornment.png" width="100%" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-xs-12 biene-mtop-mob mt-4">
                    <div class="biene-after-envrt">
                        <h3 data-aos="fade-in" data-aos-duration="1000">Environment</h3>
                        <p data-aos="fade-in" data-aos-duration="2000">Bienewabe is in an idyllic setting; sprawling campus amidst hills and fields, fine weather and a natural, pollution-free atmosphere that satisties a crucial need in today's world for students to learn, observe, introspect and contemplate
                            in a most desirable environment. Education in such an ambience is a rewarding experience for our students, truly their "wonderyears".
                        </p>
                        <p data-aos="fade-in" data-aos-duration="3000">Students start each day with an assembly during which the recitation of surahs, duas and the 99 names of Allah illuminates the school's atmosphere. Smiling and greeting with salaam through the corridors is a common practice. Teachers
                            show an appreciable realization of their teachings through their practice. The implementation of teachings is evident; students working, praying, playing or resting, they show discipline, manners and etiquettes Alhamdulillah.</p>
                        <p data-aos="fade-in" data-aos-duration="2000">Innovative soft boards reinforce teaching concepts. These are periodically updated by children on their academic and Islamic lesson activities and theme of the month information. They vibrantly add to the school environment and
                            have a significant effect on students and visitors.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Parent Role  -->
    <section class="section-50 back_bg_come">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="biene-gard-role">
                        <div class="biene-gard-sub">
                            <h3 data-aos="fade-in" data-aos-duration="2000">Parent's Role</h3>
                            <p data-aos="fade-in" data-aos-duration="3000">A number of studies conducted have shown that the active participation of both parents and educators is what makes school-home colaboration such a simple, yet vital and powerful concept. Ultimately the most powerful method
                                of influencing our children's behaviour is our relationship with them. A positive role by both the parents are essential for the right upbringing and education of the child. Bienewabe promotes strong involvement of parents
                                in their child's schooling as we believe that a child's progress depends upon the following three entities:</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6" data-aos="zoom-in" data-aos-duration="3000">
                    <div class="biene-parets-img">
                        <img src="asset/img/biene-parent-img.png" width="100%" alt="">
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="biene-our-donor">
                        <h5>We work closely with the parents of our students in order to achieve the best results.</h5>
                        <div class="biene-amaze-one">
                            <ul>
                                <li data-aos="fade-down" data-aos-duration="2000">
                                    <span>
                                        <i class="fa fa-recycle" aria-hidden="true"></i>
                                        <p>Parents are invited for an orientation session at the beginning of the
                                            academic year in order to acquaint them with the functioning, teaching
                                            methodology, assessment and other important aspects of the school.</p>
                                    </span>
                                </li>
                                <li data-aos="fade-down" data-aos-duration="3000">
                                    <span>
                                        <i class="fa fa-recycle" aria-hidden="true"></i>
                                        <p>Parents are requested to attend parent teacher meetings conducted
                                            twice every academic year.</p>     
                                        
                                    </span>
                                </li>
                                <li data-aos="fade-down" data-aos-duration="2000">
                                    <span>
                                        <i class="fa fa-recycle" aria-hidden="true"></i>
                                        <p>Parents are invited tor the various extra-curricular activities like
                                            education fair, charity week, sports day, annual day etc. to better
                                            understand the activity based learning imparted at Bienewabe.
                                            </p>     
                                        
                                    </span>
                                </li>
                                <li data-aos="fade-down" data-aos-duration="3000">
                                    <span>
                                        <i class="fa fa-recycle" aria-hidden="true"></i>
                                        <p>
                                            Tarbiyyah sessions for the parents are also a unique and rewarding
                                            experience forthe parents of Bienewabe students.</p>     
                                        
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-12 text-center">
                <div class="binen-succes">
                    <p>Successful parenting is not about accomplishing big projects like arranging a whopping amount of fees for your children's education or providing cars and branded commodities to them, but it is about succeeding in inculcating small
                        traits like good habits and self- management in your child.</p>
                </div>
            </div>
        </div>
    </section>
    <!-- Parent Role  -->
    <section class="bees-bg-type">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8">
                    <div class="bees-part-hed">
                        <div class="bees-call-easy">
                            <h3>Parenting is Easy</h3>
                            <p>You don't have to work upon your children but you have to work upon yourselves to see results in your children.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="bees-part-dash">
                        <div class="bees-call-asa">
                            <ul>
                                <li>
                                    <span>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <p>Parenting cannot be delegated</p>
                                    </span>
                                </li>
                                <li>
                                    <span>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <p>You are your child's best teacher</p>
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Staff Selection  -->
    <section class="section-50 ">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-8">
                    <div class="biene-wab-staff">
                        <h3 data-aos="fade-in" data-aos-duration="2000">Staff Development</h3>
                        <p data-aos="fade-in" data-aos-duration="3000">All teachers are well qualified and undergo ongoing, intensive training throughout the academic year. In the professional development program, teachers get a chance to learn from internal and International experts of specialist
                            areas. Staff is trained with knowledge and experience to inculcate Islamic values into the curriculum, This is done in association with Nour-el- Bayan Egypt and Burooj International.</p>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4" data-aos="zoom-in" data-aos-duration="2000">
                    <div class="biene-staff-head">
                        <div class="biene-staff-subs">
                            <img src="asset/img/biene-staff-development.png" width="100%" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Whatsapp chat  -->
    <a href="https://api.whatsapp.com/send?phone=9349157474&text=Merhaba" class="float" target="_blank">
        <i class="fa fa-whatsapp wp-button"></i>
    </a>
    <!-- Call Chat   -->
    <div class="call-us">
        <a href="tel: +919349157474">
            <img src="asset/icons/call-center.png">
        </a>
    </div>
    <?php include('links/script.php'); ?>
    <?php include('links/footer.php'); ?>
    <script>
        AOS.init();
    </script>
</body>

</html>