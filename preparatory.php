<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Bienewabe | World School | An Initative of The B School International</title>
    <meta name=description content="Bienewabe World School An Initative of The B School International, Kottakkal">
    <meta name="keywords" content="Bienewabe World School An Initative of The B School International, Kottakkal">
    <?php include('links/style.php'); ?>
</head>

<body>
    <!-- header area  -->
    <?php include('links/header.php'); ?>
    <!-- Return to Top -->
    <a href="javascript:" id="return-to-top">
        <img src="asset/icons/top-arrow.png" alt="">
    </a>
    <!-- Return to Top -->

    <!-- contact Banner image  -->
    <section class="bienewabe-preparatory">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-left">
                    <div class="bien_titl">
                        <h2>Preparatory </h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- contact Banner image  -->


    <section class="section-50 bienwall-dots">
        <div class="container">
            <div class="col-md-12 col-lg-12 text-center wing-botom">
                <div class="animate one">
                    <span>P</span><span>r</span><span>e</span><span>p</span><span>a</span><span>r</span><span>a</span><span>t</span><span>o</span><span>r</span><span>y</span>&nbsp;
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" data-aos="fade-up" data-aos-duration="2000">
                    <div class="wing-headline">
                        <h3>Preparatory ( 4 - 5 Years ) Journey to Primary School</h3>
                        <p>Students have an experience of learning the Cambridge way of Kindergarten from Prep onwards. This one year course is broken into 2 semesters. During the first semester, the student is given basics in recognizing letters, memorising
                            rhymes, juggling with numbers, stories from nature and environment, spend time with crayons and colouring. In the second semester, the student is given a second set of books to develop writing and reading skills.With other
                            additional Islamic subjects on Moral Studies, Qur'an, Hifz, basics of Arabic and Hindi languages and other Non academic and extra curricular activities, the student is able to start the journey from primary school after the
                            end of the Prep session.</p>
                        <div class="col-lg-12 col-md-12 text-center">
                            <div class="our_textdisl">
                                <div class="wing-back">
                                    <a href="index.php">Back to Home</a>
                                </div>
                                <div class="appy-btm">
                                    <button> Apply Now</button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Intention Behind  -->

    <!-- Whatsapp chat  -->
    <a href="https://api.whatsapp.com/send?phone=9349157474&text=Merhaba" class="float" target="_blank">
        <i class="fa fa-whatsapp wp-button"></i>
    </a>
    <!-- Call Chat   -->
    <div class="call-us">
        <a href="tel: +919349157474">
            <img src="asset/icons/call-center.png">
        </a>
    </div>
    <?php include('links/script.php'); ?>
    <?php include('links/footer.php'); ?>
    <script>
        AOS.init();
    </script>
</body>

</html>