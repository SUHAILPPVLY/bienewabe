<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Bienewabe | World School | An Initative of The B School International</title>
    <meta name=description content="Bienewabe World School An Initative of The B School International, Kottakkal">
    <meta name="keywords" content="Bienewabe World School An Initative of The B School International, Kottakkal">
    <?php include('links/style.php'); ?>
</head>

<body>
    <!-- header area  -->
    <?php include('links/header.php'); ?>
    <!-- Return to Top -->
    <a href="javascript:" id="return-to-top">
        <img src="asset/icons/top-arrow.png" alt="">
    </a>
    <!-- Return to Top -->

    <!-- contact Banner image  -->
    <section class="bienewabe-secondary">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-left">
                    <div class="bien_titl">
                        <h2>Higher Secondary </h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- contact Banner image  -->


    <section class="section-50 bienwall-dots">
        <div class="container">
            <div class="col-md-12 col-lg-12 text-center wing-botom">
                <div class="animate one">
                    <span>H</span><span>i</span><span>g</span><span>h</span><span>e</span><span>r</span> <span>S</span><span>e</span><span>c</span><span>o</span><span>n</span><span>d</span><span>a</span><span>r</span><span>y</span>&nbsp;
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" data-aos="fade-up" data-aos-duration="2000">
                    <div class="wing-headline">
                        <h3>Higher Secondary: (COMMERCE STREAM) </h3>
                        <h6>Tallying the balance sheets of life</h6>
                        <p>The best investment one can make is in one's own self. The balance sheet of a student who invests in education, knowledge, expertise and skills becomes priceless. After SSLC, here at BWS, a student who pursues the Commerce stream
                            need not look far beyond for graduating or post graduating because the first ever International Business School in Malabar lies at his doorstep in the very same campus. Here begins the exciting voyage to fulfill the dream of
                            a dynamic career.</p>
                        <div class="col-lg-12 col-md-12 text-center">
                            <div class="our_textdisl">
                                <div class="wing-back">
                                    <a href="index.php">Back to Home</a>
                                </div>
                                <div class="appy-btm">
                                    <button> Apply Now</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Intention Behind  -->

    <!-- Whatsapp chat  -->
    <a href="https://api.whatsapp.com/send?phone=9349157474&text=Merhaba" class="float" target="_blank">
        <i class="fa fa-whatsapp wp-button"></i>
    </a>
    <!-- Call Chat   -->
    <div class="call-us">
        <a href="tel: +919349157474">
            <img src="asset/icons/call-center.png">
        </a>
    </div>
    <?php include('links/script.php'); ?>
    <?php include('links/footer.php'); ?>
    <script>
        AOS.init();
    </script>
</body>

</html>