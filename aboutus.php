<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Bienewabe | World School | An Initative of The B School International</title>
    <meta name=description content="Bienewabe World School An Initative of The B School International, Kottakkal">
    <meta name="keywords" content="Bienewabe World School An Initative of The B School International, Kottakkal">
    <?php include('links/style.php'); ?>
</head>

<body>
    <!-- header area  -->
    <?php include('links/header.php'); ?>
    <!-- Return to Top -->
    <a href="javascript:" id="return-to-top">
        <img src="asset/icons/top-arrow.png" alt="">
    </a>
    <!-- Return to Top -->

    <!-- contact Banner image  -->
    <section class="bienewabe-about">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-left">
                    <div class="bien_titl">
                        <h2>About us</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- contact Banner image  -->
    <section class="section-50 bienwall-dots">
        <div class="container">
            <div class="col-md-12 col-lg-12 text-center">
                <div class="animate one">
                    <span>A</span><span>b</span><span>o</span><span>u</span><span>t</span> <span>u</span><span>s</span>&nbsp;
                </div>
            </div>
            <div class="row">
                <div class="col-lg-7 col-md-7">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 mt-4" data-aos="zoom-in" data-aos-duration="2000">
                            <div class="beine-img-asa">
                                <img src="asset/img/biene-about-come.01.png" width="100%" alt="">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 mt-4" data-aos="zoom-in" data-aos-duration="2000">
                            <div class="beine-img-asa beine-img-none">
                                <img src="asset/img/biene-about-come.02.png" width="100%" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-md-5 mt-4" data-aos="fade-up" data-aos-duration="2000">
                    <div class="biene-ala-inner">
                        <h2>Who We are </h2>
                        <p>Driven by the motto to let pupil Learn through experiences & experiments' we aim to promote a research based, practical learning system implem- ented through rigorous guidance. This will further promote learners think critically,
                            synthesize knowledge, reflect own thought-process and thus transform the coming generations into global citizens of high values. They will be embodying as agents of those who make common sense into common practice. </p>
                        <p>Our Philosophy is that knowledge is the most mportant thing in one's life not only beneficial for this day but the here-after as well, hence we are motivated to push the right knowledge into them the right way so as to groom them
                            become citizens off the society witha global vision.</p>

                    </div>
                </div>
            </div>
    </section>

    <section class="section-50 bienwall-dots">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-xs-12 text-center" data-aos="fade-up" data-aos-duration="2000">
                    <div class="about-vis-mis">
                        <img src="asset/img/our-about-vis-mis.png" alt="">
                        <p> Bienewabe World School, Kottakkal aims to be one of the leading schools in Malabar in eminence and excellence. It's vision is to hold tight to the ethical values and moral lifestyle practically in an attempt to create a generation
                            equipped not only with intellctual, personal, emotional and social skills but also to raise a highly talented generation of committed and responsible, self-motivated "Superior" human beings.</p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-xs-12 text-center" data-aos="fade-up" data-aos-duration="2000">
                    <div class="about-motto">
                        <img src="asset/img/our-about-motto.png" alt="">
                        <p> The Bienewabe school logo with the honeycomb is a reflection of the motto of BWS which is to gather and increase in Knowledge <span> Rabbee Zidnee Ilman Nafian</span> Oh Lord! Increase me in knowledge that is beneficial Driven
                            by this motto, the Bienewabians learn to think critically, synthesise knowledge, reflect own thought process and thus transform into global citizens with "Superior" values.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Intention Behind  -->
    <section class="section-50 bienwall-dots">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 mt-2 mb-2">
                    <div class="about-intention">
                        <div class="about-intention-sub">
                            <div class="about-intention-icon">
                                <img src="asset/icons/about-behind.png" alt="">
                                <h3>Intention Behind...</h3>
                            </div>
                            <p data-aos="fade-up" data-aos-duration="2000">The ultimate intention behind the mission is to mould a generation of leaders by transforming the students of Malabar into God-loving, active, compassionate life long learners and avail them the chance to catch up with the
                                skill sets required to be a global leader through rigorous educational methods from Pre-school levels to Post graduation.</p>
                            <p data-aos="fade-up" data-aos-duration="2000">We do this successfully by executing value based education model which continuously adds value to students from their childhood with the participation of parents, teachers and staffs so as to equip them face the challenges
                                of the competitive world.</p>
                        </div>
                        <div class="about-intention-we">
                            <h5>We at Bienewabe dedicated to equip our children to:</h5>
                            <ul>
                                <li data-aos="fade-up" data-aos-duration="1000">
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                    <span>Strive to bring up the average aspirants be at the foremost, thus bring out their actual inner talents.</span>
                                </li>
                                <li data-aos="fade-up" data-aos-duration="2000">
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                    <span>Encourage to become religiously responsible, respectful human being.</span>
                                </li>
                                <li data-aos="fade-up" data-aos-duration="3000">
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                    <span>Develop confidence, personality with a critically thinking mind and adherence to authentic Islamic values.</span>
                                </li>
                                <li data-aos="fade-up" data-aos-duration="2000">
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                    <span>Focus to build strong identitywith respect, tolerance and acceptance of diversity.</span>
                                </li>
                                <li data-aos="fade-up" data-aos-duration="3000">
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                    <span>Insist on punctuality cleanliness, respect and professionalismetc.</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 mt-2 mb-2" data-aos="zoom-in" data-aos-duration="2000">
                    <div class="beiene-serve-img">
                        <div class="beiene-serve-img-cur">
                            <img src="asset/img/biene-inetent-img.png" width="100%" alt="">
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- Whatsapp chat  -->
    <a href="https://api.whatsapp.com/send?phone=9349157474&text=Merhaba" class="float" target="_blank">
        <i class="fa fa-whatsapp wp-button"></i>
    </a>
    <!-- Call Chat   -->
    <div class="call-us">
        <a href="tel: +919349157474">
            <img src="asset/icons/call-center.png">
        </a>
    </div>
    <?php include('links/script.php'); ?>
    <?php include('links/footer.php'); ?>
    <script>
        AOS.init();
    </script>
</body>

</html>