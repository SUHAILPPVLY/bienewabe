<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Bienewabe | World School | An Initative of The B School International</title>
    <meta name=description content="Bienewabe World School An Initative of The B School International, Kottakkal">
    <meta name="keywords" content="Bienewabe World School An Initative of The B School International, Kottakkal">
    <?php include('links/style.php'); ?>
</head>

<body>
    <!-- header area  -->
    <?php include('links/header.php'); ?>
    <!-- Return to Top -->
    <a href="javascript:" id="return-to-top">
        <img src="asset/icons/top-arrow.png" alt="">
    </a>
    <!-- Return to Top -->

    <!-- contact Banner image  -->
    <section class="bienewabe-blog">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-left">
                    <div class="bien_titl">
                        <h2>Blog</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- contact Banner image  -->
    <section class="section-50">
        <div class="container">
            <div class="col-md-12 col-lg-12 text-center">
                <div class="animate one">
                    <span>B</span><span>l</span><span>o</span><span>g</span>&nbsp;
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9 col-xs-12">
                    <div class="row">
                        <div class="col-md-6 col-lg-6 mt-4" data-aos="fade-down" data-aos-duration="2000">
                            <div class="biene-blog-bar">
                                <div class="biene-blog-subs">
                                    <img src="asset/img/blog/being-blog-image01.jpg" width="100%" alt="">
                                </div>
                                <div class="biene-blog-who">
                                    <ul>
                                        <li>
                                            <span>
                                                <i class="fa fa-user" aria-hidden="true"></i>
                                                <p>Lorem Isperm</p>
                                            </span>
                                        </li>
                                        <li>
                                            <span>
                                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                                <p>01-02-2021</p>
                                            </span>
                                        </li>
                                    </ul>
                                    <div class="biene-blog-hw">
                                        <h3>Lorem Insperm</h3>
                                        <p>In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content.</p>
                                        <a class="btn-hover color-1" href="blog-more.php">Read More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6 mt-4" data-aos="fade-up" data-aos-duration="2000">
                            <div class="biene-blog-bar">
                                <div class="biene-blog-subs">
                                    <img src="asset/img/blog/being-blog-image02.jpg" width="100%" alt="">
                                </div>
                                <div class="biene-blog-who">
                                    <ul>
                                        <li>
                                            <span>
                                                <i class="fa fa-user" aria-hidden="true"></i>
                                                <p>Lorem Isperm</p>
                                            </span>
                                        </li>
                                        <li>
                                            <span>
                                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                                <p>01-02-2021</p>
                                            </span>
                                        </li>
                                    </ul>
                                    <div class="biene-blog-hw">
                                        <h3>Lorem Insperm</h3>
                                        <p>In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content.</p>
                                        <a class="btn-hover color-1" href="blog-more.php">Read More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 mt-4" data-aos="fade-down" data-aos-duration="2000">
                    <div class="bien_recent">
                        <div class="bien-inn">
                            <h2>Lorem Insperm</h2>
                            <p>Recent studies have shown that few cells live as long as the individual they belong to without renewal. A majority, if not all, the cells making up the cerebral cortex belong to this small group. The life span of some other
                                human cells are as follows: </p>
                            <a class="blogs-arae" href="news.php">Continue Reading</a>
                            <form>
                                <input class="input-area" type="text" name="search" placeholder="Search..">
                            </form>
                            <span>Recent Post</span>
                        </div>
                        <div class="recent-all">
                            <ul>
                                <li>
                                    <a href="blog-more.php"><img src="asset/img/blog/blog-recent_01.jpg" width="30%" height="50px">
                                        <p>It is a long established fact that a reader will be distracted</p>
                                        <h5>1 Jan,2020</h5>
                                    </a>
                                </li>
                                <li>
                                    <a href="blog-more.php"><img src="asset/img/blog/blog-recent_02.jpg" width="30%" height="50px">
                                        <p>It is a long established fact that a reader will be distracted</p>
                                        <h5>1 Jan,2020</h5>
                                    </a>
                                </li>
                                <li>
                                    <a href="blog-more.php"><img src="asset/img/blog/blog-recent_03.jpg" width="30%" height="50px">
                                        <p>It is a long established fact that a reader will be distracted</p>
                                        <h5>1 Jan,2020</h5>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <!-- Our focus  -->
    <!-- Whatsapp chat  -->
    <a href="https://api.whatsapp.com/send?phone=9349157474&text=Merhaba" class="float" target="_blank">
        <i class="fa fa-whatsapp wp-button"></i>
    </a>
    <!-- Call Chat   -->
    <div class="call-us">
        <a href="tel: +919349157474">
            <img src="asset/icons/call-center.png">
        </a>
    </div>
    <?php include('links/script.php'); ?>
    <?php include('links/footer.php'); ?>
    <script>
        AOS.init();
    </script>
</body>

</html>