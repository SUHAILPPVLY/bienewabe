<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Bienewabe | World School | An Initative of The B School International</title>
    <meta name=description content="Bienewabe World School An Initative of The B School International, Kottakkal">
    <meta name="keywords" content="Bienewabe World School An Initative of The B School International, Kottakkal">
    <?php include('links/style.php'); ?>
</head>

<body>
    <!-- header area  -->
    <?php include('links/header.php'); ?>
    <!-- Return to Top -->
    <a href="javascript:" id="return-to-top">
        <img src="asset/icons/top-arrow.png" alt="">
    </a>
    <!-- Return to Top -->

    <!-- contact Banner image  -->
    <section class="bienewabe-high">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-left">
                    <div class="bien_titl">
                        <h2>High School </h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- contact Banner image  -->


    <section class="section-50 bienwall-dots">
        <div class="container">
            <div class="col-md-12 col-lg-12 text-center wing-botom">
                <div class="animate one">
                    <span>H</span><span>i</span><span>g</span><span>h</span> <span>S</span><span>c</span><span>h</span><span>o</span><span>o</span><span>l</span>&nbsp;
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" data-aos="fade-up" data-aos-duration="2000">
                    <div class="wing-headline">
                        <h3>High School</h3>
                        <p>Giving focus on Medical and Engineering, students with an exposure in International curriculum can prepare for the SSLC Examination with ease and purpose. The 1st outgoing batch in High School section with full pass and 80% full
                            A plus is in itself a proof of the first test of genius and expertise of our staff in proficiently carving out our students</p>
                        <div class="col-lg-12 col-md-12 text-center">
                            <div class="our_textdisl">
                                <div class="wing-back">
                                    <a href="index.php">Back to Home</a>
                                </div>
                                <div class="appy-btm">
                                    <button> Apply Now</button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Intention Behind  -->

    <!-- Whatsapp chat  -->
    <a href="https://api.whatsapp.com/send?phone=9349157474&text=Merhaba" class="float" target="_blank">
        <i class="fa fa-whatsapp wp-button"></i>
    </a>
    <!-- Call Chat   -->
    <div class="call-us">
        <a href="tel: +919349157474">
            <img src="asset/icons/call-center.png">
        </a>
    </div>
    <?php include('links/script.php'); ?>
    <?php include('links/footer.php'); ?>
    <script>
        AOS.init();
    </script>
</body>

</html>