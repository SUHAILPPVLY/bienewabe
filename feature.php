<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Bienewabe | World School | An Initative of The B School International</title>
    <meta name=description content="Bienewabe World School An Initative of The B School International, Kottakkal">
    <meta name="keywords" content="Bienewabe World School An Initative of The B School International, Kottakkal">
    <?php include('links/style.php'); ?>
</head>

<body>
    <!-- header area  -->
    <?php include('links/header.php'); ?>
    <!-- Return to Top -->
    <a href="javascript:" id="return-to-top">
        <img src="asset/icons/top-arrow.png" alt="">
    </a>
    <!-- Return to Top -->

    <!-- contact Banner image  -->
    <section class="bienewabe-featured">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-left">
                    <div class="bien_titl">
                        <h2>Character Development</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- contact Banner image  -->
    <section class="section-1">
        <div class="container">
            <div class="col-md-12 col-lg-12 text-center">
                <div class="animate one">
                    <span>O</span><span>u</span><span>r</span> <span>F</span><span>e</span><span>a</span><span>t</span><span>u</span><span>r</span><span>e</span><span>s</span>&nbsp;
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-lg-6  col-xs-12 mt-4 mb-2">
                    <div class="biene-wabe-feature">
                        <div class="biene-chilone">
                            <h5>Character Breeds Personality</h5>
                            <p data-aos="fade-up" data-aos-duration="2000">The true wealth and worth of a person is the character.The good a person does in this world will remain till the Hereafter. Many are the ways to leave one's footprints in the sands of time. Exposure to a variety of social events
                                can empower the students and keep the mind fertile. A student's academic planning has been very carefully scheduled to include such empowerment programmes like periodic charity activities, visits to different real life
                                scenarios etc to make them involved in productive pursuits. Providing relief to the ailing and the needy, social services, a day with the eldery in the old age homes, etc are all part of learning through observation. This
                                is the legacy students will leave behind for future generations to follow. Since charity begins from home, charity programmes have prompted students and parents to collect funds on a weekly basis ( a rupee a week ). This
                                has led to the formation of the Bienewabe Charity Fund (BCF). Students are directly involved in such charity works which is undoubtedly an education beyond classroom.</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-6  col-xs-12 mt-4 mb-2" data-aos="fade-up" data-aos-duration="3000">
                    <div class="features-doem">
                        <img src="asset/img/feature/our-featured-img.png" width="100%" alt="">
                    </div>
                </div>
            </div>
    </section>
    <section class="section-1 feature_patter">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-lg-6  col-xs-12 mt-4 mb-2 order-sm-12 order-1" data-aos="fade-up" data-aos-duration="2000">
                    <div class="features-doem">
                        <img src="asset/img/feature/our-featured-traditional-music.png" width="100%" alt="">
                    </div>
                </div>
                <div class="col-sm-12 col-lg-6  col-xs-12 mt-4 mb-2 order-sm-12 order-0">
                    <div class="biene-wabe-feature bien-top-tefa">
                        <div class="biene-chilone">
                            <h5 data-aos="fade-up" data-aos-duration="2000">Keeping Heritage Alive: Folklore Traditional Music</h5>
                            <i data-aos="fade-up" data-aos-duration="2000">Real popular culture is Folk Art Noam Chomsky</i>
                            <p data-aos="fade-up" data-aos-duration="3000">Time is a witness to the fading away of many rich art, culture and heritage losing out to a digitalized era of technology. Keeping this in mind and to revive Kerala's age old culture and traditions, BWS presents exclusive classes
                                by professional trainers in Folklore Music. This helps the students not only to fondly recall Islamic traditions, in increasing the memory power through memorisation of lyrics, but also in the articulation skills as an
                                added advantage for beautiful, melodious recitation in Tajweed.</p>
                        </div>
                    </div>
                </div>

            </div>
    </section>
    <section class="section-1">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-lg-6  col-xs-12 mt-4 mb-2">
                    <div class="biene-wabe-feature bien-top-tefa">
                        <div class="biene-chilone">
                            <h5 data-aos="fade-up" data-aos-duration="2000">Environment</h5>
                            <i>Acquire knowledge, learn tranquility and dignity Omar ibn al khattab</i>
                            <p data-aos="fade-up" data-aos-duration="3000">Tranquility in learning and in its settings, BWS has been blessed with a sprawling campus amidst hills and fields, fine weather and a natural pollution-free idyllic setting. Students start each day with invocations and recitations
                                from the Holy Qur'an, morning adhkar and daily schedules assigned to each student. Another common practice of exchanging greetings with peace and a smile illuminates the school's atmosphere. The most effective learning
                                is through practice and its implementation is rewarding in a student's etiquette at school and home.</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-6  col-xs-12 mt-4 mb-2" data-aos="fade-up" data-aos-duration="3000">
                    <div class="features-doem">
                        <img src="asset/img/feature/our-featured-environment.png" width="100%" alt="">
                    </div>
                </div>
            </div>
    </section>
    <section class="section-1 feature_patter">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-lg-6  col-xs-12 order-sm-12 order-1 mt-4 mb-2" data-aos="fade-up" data-aos-duration="3000">
                    <div class="features-doem">
                        <img src="asset/img/feature/our-featured-staff-devlp.png" width="100%" alt="">
                    </div>
                </div>
                <div class="col-sm-12 col-lg-6  col-xs-12 order-sm-12 order-0 mt-4 mb-2">
                    <div class="biene-wabe-feature ">
                        <div class="biene-chilone">
                            <h5>Staff Development</h5>
                            <i>The mediocre teacher tells</i>
                            <i>The good teacher explains</i>
                            <i>The superior teacher demonstrates</i>
                            <i>The great teacher inspires</i>
                            <p data-aos="fade-up" data-aos-duration="2000">BWS expects to raise the bar of perfection by creating leaders for the new generation students. The student centred approach as against the conventional teacher centred approach ensures that BWS imparts specialized education
                                of International standards. Highly qualified teachers for Academic as well as non scholastic subjects are emphasized to develop student skills, creative thinking and their problem solving acumen for creating well balanced
                                and principled students. The international curriculum offers opportunities to explore subjects in depth in application level, through a rigorous globally recognized programme. </p>
                            <p data-aos="fade-up" data-aos-duration="3000">Every Bienewabian grows up to be a leader in his own learning process through experiencing, experimenting and exploring. BWS takes pride in keeping their staff engaged with periodical training programmes to enhance teaching
                                skills and to keep them updated.</p>
                        </div>
                    </div>
                </div>

            </div>
    </section>

    <section class="section-1">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-lg-6  col-xs-12 mt-4 mb-2">
                    <div class="biene-wabe-feature">
                        <div class="biene-chilone">
                            <h5 data-aos="fade-up" data-aos-duration="2000">Assessment and Grading</h5>
                            <p data-aos="fade-up" data-aos-duration="3000">The assessment practices of BWS promote independent learning by identifying the areas of improvement. The academic progress of the learners is systematically monitored by their teachers and supervisors through close observation
                                as well as through regular formative and summative assessments, assessments of class room activities, team work, projects, literary skills , communicative skills , presentation skills , artistic skills and athletic skills.
                                The Character and social traits of every Bienewabian is also assessed every term . Assessments occur at periodic intervals and are six, three formative assessments (FA ) and three summative assessments (SA )</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-lg-6  col-xs-12" data-aos="fade-up" data-aos-duration="2000">
                            <div class="feature-assed">
                                <h2>Formative Assessment</h2>
                                <ul>
                                    <li>
                                        <span>
                                            <i class="fa fa-circle" aria-hidden="true"></i>
                                            <h5>FA1 - July</h5>
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                            <i class="fa fa-circle" aria-hidden="true"></i>
                                            <h5>FA2 - October</h5>
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                            <i class="fa fa-circle" aria-hidden="true"></i>
                                            <h5>FA3 - February</h5>
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-6  col-xs-12 " data-aos="fade-up" data-aos-duration="3000">
                            <div class="feature-assed">
                                <h2>Summative Assessment</h2>
                                <ul>
                                    <li>
                                        <span>
                                            <i class="fa fa-circle" aria-hidden="true"></i>
                                            <h5>SA1 - August</h5>
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                            <i class="fa fa-circle" aria-hidden="true"></i>
                                            <h5>SA2 - December</h5>
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                            <i class="fa fa-circle" aria-hidden="true"></i>
                                            <h5>SA3 - March</h5>
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-sm-12 col-lg-6  col-xs-12 mt-4 mb-2" data-aos="fade-up" data-aos-duration="3000">
                    <div class="features-doem">
                        <img src="asset/img/feature/our-featured-assesment-grade.png" width="100%" alt="">
                    </div>
                </div>
            </div>
    </section>
    <!-- Whatsapp chat  -->
    <a href="https://api.whatsapp.com/send?phone=9349157474&text=Merhaba" class="float" target="_blank">
        <i class="fa fa-whatsapp wp-button"></i>
    </a>
    <!-- Call Chat   -->
    <div class="call-us">
        <a href="tel: +919349157474">
            <img src="asset/icons/call-center.png">
        </a>
    </div>
    <?php include('links/script.php'); ?>
    <?php include('links/footer.php'); ?>
    <script>
        AOS.init();
    </script>
</body>

</html>