<!-- Top Header  -->
<header class="header-area transparent">
    <div class="header-menu navigation">
        <div class="container">
            <div class="row no-gutters">
                <div class="col-lg-3 col-sm-6 col-xs-6">
                    <div class="logo">
                        <a href="index.php"><img src="asset/img/bienewabe-logo.png" class="logo" alt="logo"> </a>
                    </div>
                </div>
                <div class="col-lg-9 col-sm-12 col-xs-12">
                    <div class="main-menu text-right">
                        <nav id="mobile-menu">
                            <ul class="nav-menubar" id="nav">
                                <li class="active">
                                    <a href="index.php" class="nav-sub-link">Home</a>
                                </li>
                                <li>
                                    <a href="aboutus.php" class="nav-sub-link">About us</a>
                                </li>
                                <li>
                                    <a href="we-cources.php" class="nav-sub-link">What We Provide ?</a>
                                </li>
                                <li>
                                    <a href="curriculum.php" class="nav-sub-link">Curriculum</a>
                                </li>
                                <li>
                                    <a href="feature.php" class="nav-sub-link">Our Feature</a>
                                </li>
                                <li>
                                    <a href="gallery.php" class="nav-sub-link">Gallery</a>
                                </li>

                                <li>
                                    <a href="blog.php" class="nav-sub-link">Blog</a>
                                </li>
                                <li class="mean-bor-none">
                                    <a href="contact.php" class="nav-sub-link">Contact us</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <!-- main menu -->
                    <div class="mobile-menu"></div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- Top Header End  -->
<!-- Course Pop Form Button  -->
<div class="pop-up">
    <div class="content">
        <div class="dots">
            <div class="dot"></div>
            <div class="dot"></div>
            <div class="dot"></div>
            <div class="dot"></div>
        </div>
        <span class="close">
                <i class="fa fa-times-circle fas-awa" aria-hidden="true" alt="Close"></i>
            </span>
        <div class="form-beine">
            <div class="form-dades">
                <h3>Enquiry Form</h3>
            </div>
            <form id="contact" action="" method="post">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <input class="input-haw" placeholder="Enter Name" type="text" tabindex="1" required>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <input class="input-haw" placeholder="Enter Place" type="text" tabindex="1" required>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <input class="input-haw" name="Number" placeholder="Mobile Number" type="text" tabindex="1" required>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <input class="input-haw" placeholder="E-mail" type="text" tabindex="1" required>
                    </div>
                    <div class="col-lg-12 col-md-12">
                        <div class="course_dropdown">
                            <div class="select">
                                <span>Select Course</span>
                                <i class="fa fa-chevron-left"></i>
                            </div>
                            <input type="hidden" name="gender">
                            <ul class="dropdown-menu">
                                <li id="Preschool">Preschool</li>
                                <li id="School of Hitz">School of Hitz</li>
                                <li id="Preparatory">Preparatory </li>
                                <li id="Preparatory">Lower and Upper Primary </li>
                                <li id="Preparatory">High School </li>
                                <li id="Preparatory">Higher Secondary </li>

                            </ul>
                        </div>

                        <!-- <input class="input-haw" placeholder="E-mail" type="text" tabindex="1" required> -->
                    </div>
                    <div class="col-lg-12 col-md-12">
                        <textarea class="input-area" rows="4" cols="50" placeholder=" Your Message..."></textarea>
                    </div>
                    <div class="col-lg-12 col-md-12 mt-4 text-center">
                        <div class="appy-btm">
                            <button> Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Course Pop Form Button  -->