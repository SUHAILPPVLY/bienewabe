<!--main jquery-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="asset/js/jquery.meanmenu.min.js"></script>
<!-- Slider js  -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.15.1/TweenMax.min.js"></script>
<script src="asset/js/slider.js"></script>
<!--slider-slick-->
<!-- <script src="https://kenwheeler.github.io/slick/slick/slick.js"></script> -->
<!--main js-->
<script src="asset/js/script.js"></script>

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
<script src="asset/js/portfolio.popup.js"></script>
<!-- Home Gallery  -->
<!-- <script src="asset/js/wings.section.js"></script> -->
<!-- wings Popup   -->

<script src="asset/js/courses-popnow.js"></script>
<!-- Popop Youtube  -->
<script src="asset/js/our-prospective-vedio.js"></script>

<!-- testimonial js  -->
<script src="//cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.1/owl.carousel.min.js"></script>
<script src="asset/js/testimonial-section.js"></script>

<!--topbutton-->
<script src="asset/js/top-button.js"></script>

<!--aos animated js-->
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script src="asset/js/wow.js"></script>
<script src="asset/js/footer-smooth-scroll.js"></script>