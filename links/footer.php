<!--footer area start-->
<footer class="footer-area" data-aos="fade-down" data-aos-duration="2000">
    <div class="container">
        <div class="row footer-bottom-border">
            <div class="col-lg-3 col-md-3 col-sm-6 ">
                <div class="beine-single-widget-fa">
                    <a href="index.php">
                        <img src="asset/img/bienewabe-footer-logo.png" alt="Footer Logo">
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="beine-single-widget-fa biene-single-margin">
                    <h2>Contact info</h2>
                    <ul class="footer-contact">
                        <li class="footer-border">
                            <span>
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <p>Bienewabe World School, Kottakkal <br>Pin: 676501, Kerala </p>
                            </span>
                        </li>
                        <li class="footer-border">
                            <span>
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <a href="tel:+919349157474">+91 9349 157 474</a>
                           </span>
                        </li>
                        <li class="footer-border">
                            <span>
                                <i class="fa fa-envelope" aria-hidden="true"></i>
                                <a href="mailto:bienewabe.kkl@gmail.com">bienewabe.kkl@gmail.com</a>
                            </span>
                        </li>

                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                <div class="single-widget-c">
                    <h2>Quick Link</h2>
                    <ul class="footer-contact">
                        <li class="beine-bottom-link">
                            <span>
                                <i class="fa fa-dot-circle-o" aria-hidden="true"></i>
                                <!-- <a href="aboutus.php">About us</a> -->
                                <a href="aboutus.php">About us</a>
                           </span>
                        </li>
                        <li class="beine-bottom-link">
                            <span>
                                <i class="fa fa-dot-circle-o" aria-hidden="true"></i>
                                <a href="blog.php">Blog</a>
                            </span>
                        </li>
                        <li class="beine-bottom-link">
                            <span>
                                <i class="fa fa-dot-circle-o" aria-hidden="true"></i>
                                <a href="we-cources.php">Courses</a>
                            </span>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                <div class="beine-single-widget-fa">
                    <h2>More Link</h2>
                    <ul class="footer-contact">
                        <li class="beine-bottom-fas">
                            <span>
                                <a href="https://www.facebook.com/Bienewabe" target="_blank">
                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                    <p>Facebook</p>
                                </a>
                           </span>
                        </li>
                        <li class="beine-bottom-fas">
                            <span>
                                <a href="https://www.instagram.com/Bienewabe" target="_blank">
                                    <i class="fa fa-instagram " aria-hidden="true"></i>
                                    <p>instagram</p>
                                </a>
                            </span>
                        </li>
                        <li class="beine-bottom-fas">
                            <span>
                                <a href="https://www.youtube.com/Bienewabe" target="_blank">
                                    <i class="fa fa-youtube" aria-hidden="true"></i>
                                    <p>Youtube</p>
                                </a>
                            </span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-bay">
            <div class="col-lg-12 col-md-12 text-center">
                <div class="beine-bottom">
                    <span>Copyright 2020 © Bienewabe | World School. All Rights Reserved
                                <a href="http://www.d5ndigital.com" target="_blank"><img src="asset/icons/d5n-icon.png"></a>
                            </span>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--footer area end-->