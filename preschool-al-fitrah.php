<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Bienewabe | World School | An Initative of The B School International</title>
    <meta name=description content="Bienewabe World School An Initative of The B School International, Kottakkal">
    <meta name="keywords" content="Bienewabe World School An Initative of The B School International, Kottakkal">
    <?php include('links/style.php'); ?>
</head>

<body>
    <!-- header area  -->
    <?php include('links/header.php'); ?>
    <!-- Return to Top -->
    <a href="javascript:" id="return-to-top">
        <img src="asset/icons/top-arrow.png" alt="">
    </a>
    <!-- Return to Top -->

    <!-- contact Banner image  -->
    <section class="bienewabe-preschool">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-left">
                    <div class="bien_titl">
                        <h2>Preschool </h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- contact Banner image  -->


    <section class="section-50 bienwall-dots">
        <div class="container">
            <div class="col-md-12 col-lg-12 text-center wing-botom">
                <div class="animate one">
                    <span>P</span><span>r</span><span>e</span><span>s</span><span>c</span><span>h</span><span>o</span><span>o</span><span>l</span>&nbsp;
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" data-aos="fade-up" data-aos-duration="2000">
                    <div class="wing-headline">
                        <h3>Al Fitrah (3 - 6 Years) (EGYPTIAN : Nur ul Bayan Wal Fathu Rabbani)</h3>
                        <p>Affiliated to the Egyptian curriculum,<b> This most applauded futuristic preschool </b> functions under BWS. Known for its innovative, novel and unconventional training style, the Nur ul Bayan wal Fathu Rabbani curriculum has gained
                            recognition in Kerala also and a number of such affiliates have sprung up. This edutainment programme enables the child to recite ( Tilawatul Qur'an ) in accordance with the traditions by the time the course is completed. In
                            3 years time, our preschooler memorize 2 juz of the Holy Quran and numerous khatms. Our preschoolers become <b> Little scholars</b> and are able to recite from any portion of the Qur'an with proper Tajweed. Al Fitrah gives
                            equal importance to other scholastic areas also including English language ( with phonics ), Maths, EVS, Islamic Studies. Under BWS, non scholastic areas are in full bloom with Annual Day ( NI'MA )stage performances, Annual
                            Sports Meet ( Ranang), Exhibitions ( Eureka ), excursions and field visits etc. 2 well - trained teachers for a class of limited strength of 24 students and an Auntie are the highlights of Al Fitrah.</p>
                        <div class="col-lg-12 col-md-12 text-center">
                            <div class="our_textdisl">
                                <div class="wing-back">
                                    <a href="index.php">Back to Home</a>
                                </div>
                                <div class="appy-btm">
                                    <button> Apply Now</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Intention Behind  -->

    <!-- Whatsapp chat  -->
    <a href="https://api.whatsapp.com/send?phone=9349157474&text=Merhaba" class="float" target="_blank">
        <i class="fa fa-whatsapp wp-button"></i>
    </a>
    <!-- Call Chat   -->
    <div class="call-us">
        <a href="tel: +919349157474">
            <img src="asset/icons/call-center.png">
        </a>
    </div>
    <?php include('links/script.php'); ?>
    <?php include('links/footer.php'); ?>
    <script>
        AOS.init();
    </script>
</body>

</html>