// Our wing 
;

$('#customers-testimonials-wing').owlCarousel({
    loop: true,
    margin: 10,
    items: 3,
    responsiveClass: true,
    responsive: {
        0: {
            items: 1,
            nav: true
        },
        600: {
            items: 1,
            nav: false
        },
        1000: {
            items: 3,
            nav: true,
            loop: false
        }
    }
})


// customers-testimonials
;

$('#customers-testimonials').owlCarousel({
    loop: true,
    margin: 10,
    items: 4,
    responsiveClass: true,
    responsive: {
        0: {
            items: 1,
            nav: true
        },
        600: {
            items: 1,
            nav: false
        },
        1000: {
            items: 3,
            nav: true,
            loop: false
        }
    }
})