function closeForm() {
    $('.form-popup-bg').removeClass('is-visible');
}

$(document).ready(function($) {

    /* Contact Form Interactions */
    $('#btnOpenForm').on('click', function(event) {
        event.preventDefault();

        $('.form-popup-bg').addClass('is-visible');
    });

    //close popup when clicking x or off popup
    $('.form-popup-bg').on('click', function(event) {
        if ($(event.target).is('.form-popup-bg') || $(event.target).is('#btnCloseForm')) {
            event.preventDefault();
            $(this).removeClass('is-visible');
        }
    });

});

// Section 2
function closeForm() {
    $('.form-popup-bg2').removeClass('is-visible');
}

$(document).ready(function($) {

    /* Contact Form Interactions */
    $('#btnOpenForm').on('click', function(event) {
        event.preventDefault();

        $('.form-popup-bg2').addClass('is-visible');
    });

    //close popup when clicking x or off popup
    $('.form-popup-bg2').on('click', function(event) {
        if ($(event.target).is('.form-popup-bg2') || $(event.target).is('#btnCloseForm')) {
            event.preventDefault();
            $(this).removeClass('is-visible');
        }
    });

});
//second form
// $(document).ready(function() {
//     $('.popup-btn').click(function() {
//         var popupBlock = $('#' + $(this).data('popup'));
//         popupBlock.addClass('active')
//             .find('.fade-out').click(function() {
//                 popupBlock.css('opacity', '0').find('.popup-content').css('margin-top', '350px');
//                 setTimeout(function() {
//                     $('.popup').removeClass('active');
//                     popupBlock.css('opacity', '').find('.popup-content').css('margin-top', '');
//                 }, 600);
//             });
//     });
// });