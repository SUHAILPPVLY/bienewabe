var $owl = $(".owl-carousels");

$owl.children().each(function(index) {
    $(this).attr("data-position", index); // NB: .attr() instead of .data()
});

$owl.owlCarousels({
    center: true,
    loop: true,
    items: 3
});

$(document).on("click", ".owl-item>div", function() {
    // see https://owlcarousel2.github.io/OwlCarousel2/docs/api-events.html#to-owl-carousel
    var $speed = 300; // in ms
    $owl.trigger("to.owl-carousels", [$(this).data("position"), $speed]);
});