//===== menu bar area
$(function() {

    "use strict";

    $(window).on('scroll', function(event) {
        var scroll = $(window).scrollTop();
        if (scroll < 110) {
            $(".navigation").removeClass("sticky");
        } else {
            $(".navigation").addClass("sticky");
        }
    });

    //===== Meanmenu Responsive

    $('#mobile-menu').meanmenu({
        meanMenuContainer: '.mobile-menu',
        meanScreenWidth: "991",
        meanMenuClose: '<span></span><span></span></span><span></span>',
        meanMenuCloseSize: "0px",
    });

});


//sub tittle
function showSubTitle() {
    anime({
        targets: '#sub-title',
        easing: 'easeInQuad',
        opacity: 1,
        duration: 300,
        delay: 1
    });
}
// Select Dropbox Course
/*Dropdown Menu*/
$('.course_dropdown').click(function() {
    $(this).attr('tabindex', 1).focus();
    $(this).toggleClass('active');
    $(this).find('.dropdown-menu').slideToggle(300);
});
$('.course_dropdown').focusout(function() {
    $(this).removeClass('active');
    $(this).find('.dropdown-menu').slideUp(300);
});
$('.course_dropdown .dropdown-menu li').click(function() {
    $(this).parents('.course_dropdown').find('span').text($(this).text());
    $(this).parents('.course_dropdown').find('input').attr('value', $(this).attr('id'));
});
/*End Dropdown Menu*/


$('.dropdown-menu li').click(function() {
    var input = '<strong>' + $(this).parents('.course_dropdown').find('input').val() + '</strong>',
        msg = '<span class="msg">Hidden input value: ';
    $('.msg').html(msg + input + '</span>');
});