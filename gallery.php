<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Bienewabe | World School | An Initative of The B School International</title>
    <meta name=description content="Bienewabe World School An Initative of The B School International, Kottakkal">
    <meta name="keywords" content="Bienewabe World School An Initative of The B School International, Kottakkal">
    <?php include('links/style.php'); ?>
</head>

<body>
    <!-- header area  -->
    <?php include('links/header.php'); ?>
    <!-- Return to Top -->
    <a href="javascript:" id="return-to-top">
        <img src="asset/icons/top-arrow.png" alt="">
    </a>
    <!-- Return to Top -->

    <!-- contact Banner image  -->
    <section class="bienewabe-folio">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-left">
                    <div class="bien_titl">
                        <h2>Gallery</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- contact Banner image  -->
    <section class="section-50" id="gallery">
        <div class="container">
            <div class="col-md-12 col-lg-12 text-center">
                <div class="animate one">
                    <span>G</span><span>a</span><span>l</span><span>l</span><span>e</span><span>r</span><span>y</span>&nbsp;
                </div>
            </div>

            <div class="img-gallery-magnific">
                <div class="magnific-img" data-aos="fade-up" data-aos-duration="2000">
                    <div class="magfic-anim">
                        <a class="image-popup-vertical-fit" href="asset/img/portfolio/portfolio-img-static-zoom-01.png" title="image">
                            <img src="asset/img/portfolio/portfolio-img-static-01.png" width="100%" alt="image" />
                            <div class="box-content"></div>
                        </a>
                    </div>
                </div>
                <div class="magnific-img" data-aos="fade-up" data-aos-duration="3000">
                    <div class="magfic-anim">
                        <a class="image-popup-vertical-fit" href="asset/img/portfolio/portfolio-img-static-zoom-02.png" title="image">
                            <img src="asset/img/portfolio/portfolio-img-static-02.png" width="100%" alt="image" />
                            <div class="box-content"></div>
                        </a>
                    </div>
                </div>
                <div class="magnific-img" data-aos="fade-up" data-aos-duration="2000">
                    <div class="magfic-anim">
                        <a class="image-popup-vertical-fit" href="asset/img/portfolio/portfolio-img-static-zoom-03.png" title="image">
                            <img src="asset/img/portfolio/portfolio-img-static-03.png" width="100%" alt="image" />
                            <div class="box-content"></div>
                        </a>
                    </div>
                </div>
                <div class="magnific-img" data-aos="fade-up" data-aos-duration="3000">
                    <div class="magfic-anim">
                        <a class="image-popup-vertical-fit" href="asset/img/portfolio/portfolio-img-static-zoom-04.png" title="image">
                            <img src="asset/img/portfolio/portfolio-img-static-04.png" width="100%" alt="image" />
                            <div class="box-content"></div>
                        </a>
                    </div>
                </div>
                <div class="magnific-img" data-aos="fade-up" data-aos-duration="2000">
                    <div class="magfic-anim">
                        <a class="image-popup-vertical-fit" href="asset/img/portfolio/portfolio-img-static-zoom-05.png" title="image">
                            <img src="asset/img/portfolio/portfolio-img-static-05.png" width="100%" alt="image" />
                            <div class="box-content"></div>
                        </a>
                    </div>
                </div>
                <div class="magnific-img" data-aos="fade-up" data-aos-duration="3000">
                    <div class="magfic-anim">
                        <a class="image-popup-vertical-fit" href="asset/img/portfolio/portfolio-img-static-zoom-06.png" title="image">
                            <img src="asset/img/portfolio/portfolio-img-static-06.png" width="100%" alt="image" />
                            <div class="box-content"></div>
                        </a>
                    </div>
                </div>
                <div class="magnific-img" data-aos="fade-up" data-aos-duration="2000">
                    <div class="magfic-anim">
                        <a class="image-popup-vertical-fit" href="asset/img/portfolio/portfolio-img-static-zoom-07.png" title="image">
                            <img src="asset/img/portfolio/portfolio-img-static-07.png" width="100%" alt="image" />
                            <div class="box-content"></div>
                        </a>
                    </div>
                </div>
                <div class="magnific-img" data-aos="fade-up" data-aos-duration="3000">
                    <div class="magfic-anim">
                        <a class="image-popup-vertical-fit" href="asset/img/portfolio/portfolio-img-static-zoom-08.png" title="image">
                            <img src="asset/img/portfolio/portfolio-img-static-08.png" width="100%" alt="image" />
                            <div class="box-content"></div>
                        </a>
                    </div>
                </div>
                <div class="magnific-img" data-aos="fade-up" data-aos-duration="2000">
                    <div class="magfic-anim">
                        <a class="image-popup-vertical-fit" href="asset/img/portfolio/portfolio-img-static-zoom-09.png" title="image">
                            <img src="asset/img/portfolio/portfolio-img-static-09.png" width="100%" alt="image" />
                            <div class="box-content"></div>
                        </a>
                    </div>
                </div>
                <div class="magnific-img" data-aos="fade-up" data-aos-duration="3000">
                    <div class="magfic-anim">
                        <a class="image-popup-vertical-fit" href="asset/img/portfolio/portfolio-img-static-zoom-10.png" title="image">
                            <img src="asset/img/portfolio/portfolio-img-static-10.png" width="100%" alt="image" />
                            <div class="box-content"></div>
                        </a>
                    </div>
                </div>
                <div class="magnific-img" data-aos="fade-up" data-aos-duration="2000">
                    <div class="magfic-anim">
                        <a class="image-popup-vertical-fit" href="asset/img/portfolio/portfolio-img-static-zoom-11.png" title="image">
                            <img src="asset/img/portfolio/portfolio-img-static-11.png" width="100%" alt="image" />
                            <div class="box-content"></div>
                        </a>
                    </div>
                </div>
                <div class="magnific-img" data-aos="fade-up" data-aos-duration="3000">
                    <div class="magfic-anim">
                        <a class="image-popup-vertical-fit" href="asset/img/portfolio/portfolio-img-static-zoom-12.png" title="image">
                            <img src="asset/img/portfolio/portfolio-img-static-12.png" width="100%" alt="image" />
                            <div class="box-content"></div>
                        </a>
                    </div>
                </div>
                <div class="magnific-img" data-aos="fade-up" data-aos-duration="3000">
                    <div class="magfic-anim">
                        <a class="image-popup-vertical-fit" href="asset/img/portfolio/portfolio-img-static-zoom-13.png" title="image">
                            <img src="asset/img/portfolio/portfolio-img-static-13.png" width="100%" alt="image" />
                            <div class="box-content"></div>
                        </a>
                    </div>
                </div>
                <div class="magnific-img" data-aos="fade-up" data-aos-duration="3000">
                    <div class="magfic-anim">
                        <a class="image-popup-vertical-fit" href="asset/img/portfolio/portfolio-img-static-zoom-14.png" title="image">
                            <img src="asset/img/portfolio/portfolio-img-static-14.png" width="100%" alt="image" />
                            <div class="box-content"></div>
                        </a>
                    </div>
                </div>
                <div class="magnific-img" data-aos="fade-up" data-aos-duration="3000">
                    <div class="magfic-anim">
                        <a class="image-popup-vertical-fit" href="asset/img/portfolio/portfolio-img-static-zoom-15.png" title="image">
                            <img src="asset/img/portfolio/portfolio-img-static-15.png" width="100%" alt="image" />
                            <div class="box-content"></div>
                        </a>
                    </div>
                </div>
                <div class="magnific-img" data-aos="fade-up" data-aos-duration="3000">
                    <div class="magfic-anim">
                        <a class="image-popup-vertical-fit" href="asset/img/portfolio/portfolio-img-static-zoom-16.png" title="image">
                            <img src="asset/img/portfolio/portfolio-img-static-16.png" width="100%" alt="image" />
                            <div class="box-content"></div>
                        </a>
                    </div>
                </div>
                <div class="magnific-img" data-aos="fade-up" data-aos-duration="3000">
                    <div class="magfic-anim">
                        <a class="image-popup-vertical-fit" href="asset/img/portfolio/portfolio-img-static-zoom-17.png" title="image">
                            <img src="asset/img/portfolio/portfolio-img-static-17.png" width="100%" alt="image" />
                            <div class="box-content"></div>
                        </a>
                    </div>
                </div>
                <div class="magnific-img" data-aos="fade-up" data-aos-duration="3000">
                    <div class="magfic-anim">
                        <a class="image-popup-vertical-fit" href="asset/img/portfolio/portfolio-img-static-zoom-18.png" title="image">
                            <img src="asset/img/portfolio/portfolio-img-static-18.png" width="100%" alt="image" />
                            <div class="box-content"></div>
                        </a>
                    </div>
                </div>
                <div class="magnific-img" data-aos="fade-up" data-aos-duration="3000">
                    <div class="magfic-anim">
                        <a class="image-popup-vertical-fit" href="asset/img/portfolio/portfolio-img-static-zoom-19.png" title="image">
                            <img src="asset/img/portfolio/portfolio-img-static-19.png" width="100%" alt="image" />
                            <div class="box-content"></div>
                        </a>
                    </div>
                </div>
                <div class="magnific-img" data-aos="fade-up" data-aos-duration="3000">
                    <div class="magfic-anim">
                        <a class="image-popup-vertical-fit" href="asset/img/portfolio/portfolio-img-static-zoom-20.png" title="image">
                            <img src="asset/img/portfolio/portfolio-img-static-20.png" width="100%" alt="image" />
                            <div class="box-content"></div>
                        </a>
                    </div>
                </div>
                <div class="magnific-img" data-aos="fade-up" data-aos-duration="3000">
                    <div class="magfic-anim">
                        <a class="image-popup-vertical-fit" href="asset/img/portfolio/portfolio-img-static-zoom-21.png" title="image">
                            <img src="asset/img/portfolio/portfolio-img-static-21.png" width="100%" alt="image" />
                            <div class="box-content"></div>
                        </a>
                    </div>
                </div>
                <div class="magnific-img" data-aos="fade-up" data-aos-duration="3000">
                    <div class="magfic-anim">
                        <a class="image-popup-vertical-fit" href="asset/img/portfolio/portfolio-img-static-zoom-22.png" title="image">
                            <img src="asset/img/portfolio/portfolio-img-static-22.png" width="100%" alt="image" />
                            <div class="box-content"></div>
                        </a>
                    </div>
                </div>
                <div class="magnific-img" data-aos="fade-up" data-aos-duration="3000">
                    <div class="magfic-anim">
                        <a class="image-popup-vertical-fit" href="asset/img/portfolio/portfolio-img-static-zoom-23.png" title="image">
                            <img src="asset/img/portfolio/portfolio-img-static-23.png" width="100%" alt="image" />
                            <div class="box-content"></div>
                        </a>
                    </div>
                </div>
                <div class="magnific-img" data-aos="fade-up" data-aos-duration="3000">
                    <div class="magfic-anim">
                        <a class="image-popup-vertical-fit" href="asset/img/portfolio/portfolio-img-static-zoom-24.png" title="image">
                            <img src="asset/img/portfolio/portfolio-img-static-24.png" width="100%" alt="image" />
                            <div class="box-content"></div>
                        </a>
                    </div>
                </div>

            </div>
            <div class="clear"></div>
    </section>

    <!-- Whatsapp chat  -->
    <a href="https://api.whatsapp.com/send?phone=9349157474&text=Merhaba" class="float" target="_blank">
        <i class="fa fa-whatsapp wp-button"></i>
    </a>
    <!-- Call Chat   -->
    <div class="call-us">
        <a href="tel: +919349157474">
            <img src="asset/icons/call-center.png">
        </a>
    </div>
    <?php include('links/script.php'); ?>
    <?php include('links/footer.php'); ?>
    <script>
        AOS.init();
    </script>
</body>

</html>