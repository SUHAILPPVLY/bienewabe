<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Bienewabe | World School | An Initative of The B School International</title>
    <meta name=description content="Bienewabe World School An Initative of The B School International, Kottakkal">
    <meta name="keywords" content="Bienewabe World School An Initative of The B School International, Kottakkal">
    <?php include('links/style.php'); ?>
</head>

<body>
    <!-- header area  -->
    <?php include('links/header.php'); ?>
    <!-- Return to Top -->
    <a href="javascript:" id="return-to-top">
        <img src="asset/icons/top-arrow.png" alt="">
    </a>
    <!-- Return to Top -->

    <!-- contact Banner image  -->
    <section class="bienewabe-courses">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-left">
                    <div class="bien_titl">
                        <h2>Courses</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- contact Banner image  -->
    <section class="section-50">
        <div class="container">
            <div class="col-md-12 col-lg-12 text-center wing-botom">
                <div class="animate one">
                    <span>C</span><span>o</span><span>u</span><span>r</span><span>s</span><span>e</span><span>s</span>&nbsp;
                </div>
            </div>
            <!-- <div class="we_dona">
                <div class="we-cource-hw">
                    <h4>Subjects offered in IGCSE Curriculum :</h4>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-xs-12 mt-3 m-b" data-aos="fade-up" data-aos-duration="2000">
                        <div class="beine-course-head">
                            <div class="beine-course-img">
                                <img src="asset/img/courses/courses-img-slack.01.png" width="100%" alt="">
                            </div>
                            <div class="beine-pad-head">
                                <div class="beine-course-sub">
                                    <div class="beine-course-title">
                                        <h3>English</h3>
                                        <p>It was popularised of Letraset Aldus PageMaker including versions of Lorem Ipsum</p>
                                    </div>
                                </div>
                                <div class="beine-course-ul">
                                    <ul>
                                        <li>
                                            <i class="fa fa-certificate" aria-hidden="true"></i>
                                            <span>Name : <fieldset>asdas</fieldset> </span>
                                        </li>
                                        <li>
                                            <i class="fa fa-certificate" aria-hidden="true"></i>
                                            <span>Class: <fieldset>asdas</fieldset>  </span>
                                        </li>
                                        <li>
                                            <i class="fa fa-certificate" aria-hidden="true"></i>
                                            <span>Subject: <fieldset>asdas</fieldset> </span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="appy-btm">
                                    <button> Apply Now</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-xs-12 mt-3 m-b" data-aos="fade-up" data-aos-duration="3000">
                        <div class="beine-course-head">
                            <div class="beine-course-img">
                                <img src="asset/img/courses/courses-img-slack.02.png" width="100%" alt="">
                            </div>
                            <div class="beine-pad-head">
                                <div class="beine-course-sub">
                                    <div class="beine-course-title">
                                        <h3>Mathematics</h3>
                                        <p>It was popularised of Letraset Aldus PageMaker including versions of Lorem Ipsum</p>
                                    </div>
                                </div>
                                <div class="beine-course-ul">
                                    <ul>
                                        <li>
                                            <i class="fa fa-certificate" aria-hidden="true"></i>
                                            <span>Name : <fieldset>asdas</fieldset> </span>
                                        </li>
                                        <li>
                                            <i class="fa fa-certificate" aria-hidden="true"></i>
                                            <span>Class: <fieldset>asdas</fieldset>  </span>
                                        </li>
                                        <li>
                                            <i class="fa fa-certificate" aria-hidden="true"></i>
                                            <span>Subject: <fieldset>asdas</fieldset> </span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="appy-btm">
                                    <button> Apply Now</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-xs-12 mt-3 m-b" data-aos="fade-up" data-aos-duration="2000">
                        <div class="beine-course-head">
                            <div class="beine-course-img">
                                <img src="asset/img/courses/courses-img-slack.03.png" width="100%" alt="">
                            </div>
                            <div class="beine-pad-head">
                                <div class="beine-course-sub">
                                    <div class="beine-course-title">
                                        <h3>Science</h3>
                                        <p>It was popularised of Letraset Aldus PageMaker including versions of Lorem Ipsum</p>
                                    </div>
                                </div>
                                <div class="beine-course-ul">
                                    <ul>
                                        <li>
                                            <i class="fa fa-certificate" aria-hidden="true"></i>
                                            <span>Name : <fieldset>asdas</fieldset> </span>
                                        </li>
                                        <li>
                                            <i class="fa fa-certificate" aria-hidden="true"></i>
                                            <span>Class: <fieldset>asdas</fieldset>  </span>
                                        </li>
                                        <li>
                                            <i class="fa fa-certificate" aria-hidden="true"></i>
                                            <span>Subject: <fieldset>asdas</fieldset> </span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="appy-btm">
                                    <button> Apply Now</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-xs-12 mt-3 m-b" data-aos="fade-up" data-aos-duration="3000">
                        <div class="beine-course-head">
                            <div class="beine-course-img">
                                <img src="asset/img/courses/courses-img-slack.04.png" width="100%" alt="">
                            </div>
                            <div class="beine-pad-head">
                                <div class="beine-course-sub">
                                    <div class="beine-course-title">
                                        <h3>Social Studies</h3>
                                        <p>It was popularised of Letraset Aldus PageMaker including versions of Lorem Ipsum</p>
                                    </div>
                                </div>
                                <div class="beine-course-ul">
                                    <ul>
                                        <li>
                                            <i class="fa fa-certificate" aria-hidden="true"></i>
                                            <span>Name : <fieldset>asdas</fieldset> </span>
                                        </li>
                                        <li>
                                            <i class="fa fa-certificate" aria-hidden="true"></i>
                                            <span>Class: <fieldset>asdas</fieldset>  </span>
                                        </li>
                                        <li>
                                            <i class="fa fa-certificate" aria-hidden="true"></i>
                                            <span>Subject: <fieldset>asdas</fieldset> </span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="appy-btm">
                                    <button> Apply Now</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="we_dona">
                <div class="we-cource-hw">
                    <h4>Languages </h4>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-xs-12 mt-3 m-b" data-aos="fade-up" data-aos-duration="2000">
                        <div class="beine-course-head">
                            <div class="beine-course-img">
                                <img src="asset/img/courses/courses-img-slack.05.png" width="100%" alt="">
                            </div>
                            <div class="beine-pad-head">
                                <div class="beine-course-sub">
                                    <div class="beine-course-title">
                                        <h3>Malayalam</h3>
                                        <p>It was popularised of Letraset Aldus PageMaker including versions of Lorem Ipsum</p>
                                    </div>
                                </div>
                                <div class="beine-course-ul">
                                    <ul>
                                        <li>
                                            <i class="fa fa-certificate" aria-hidden="true"></i>
                                            <span>Name : <fieldset>asdas</fieldset> </span>
                                        </li>
                                        <li>
                                            <i class="fa fa-certificate" aria-hidden="true"></i>
                                            <span>Class: <fieldset>asdas</fieldset>  </span>
                                        </li>
                                        <li>
                                            <i class="fa fa-certificate" aria-hidden="true"></i>
                                            <span>Subject: <fieldset>asdas</fieldset> </span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="appy-btm">
                                    <button> Apply Now</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-xs-12 mt-3 m-b" data-aos="fade-up" data-aos-duration="3000">
                        <div class="beine-course-head">
                            <div class="beine-course-img">
                                <img src="asset/img/courses/courses-img-slack.06.png" width="100%" alt="">
                            </div>
                            <div class="beine-pad-head">
                                <div class="beine-course-sub">
                                    <div class="beine-course-title">
                                        <h3>Arabic</h3>
                                        <p>It was popularised of Letraset Aldus PageMaker including versions of Lorem Ipsum</p>
                                    </div>
                                </div>
                                <div class="beine-course-ul">
                                    <ul>
                                        <li>
                                            <i class="fa fa-certificate" aria-hidden="true"></i>
                                            <span>Name : <fieldset>asdas</fieldset> </span>
                                        </li>
                                        <li>
                                            <i class="fa fa-certificate" aria-hidden="true"></i>
                                            <span>Class: <fieldset>asdas</fieldset>  </span>
                                        </li>
                                        <li>
                                            <i class="fa fa-certificate" aria-hidden="true"></i>
                                            <span>Subject: <fieldset>asdas</fieldset> </span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="appy-btm">
                                    <button> Apply Now</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-xs-12 mt-3 m-b" data-aos="fade-up" data-aos-duration="2000">
                        <div class="beine-course-head">
                            <div class="beine-course-img">
                                <img src="asset/img/courses/courses-img-slack.07.png" width="100%" alt="">
                            </div>
                            <div class="beine-pad-head">
                                <div class="beine-course-sub">
                                    <div class="beine-course-title">
                                        <h3>Hindi</h3>
                                        <p>It was popularised of Letraset Aldus PageMaker including versions of Lorem Ipsum</p>
                                    </div>
                                </div>
                                <div class="beine-course-ul">
                                    <ul>
                                        <li>
                                            <i class="fa fa-certificate" aria-hidden="true"></i>
                                            <span>Name : <fieldset>asdas</fieldset> </span>
                                        </li>
                                        <li>
                                            <i class="fa fa-certificate" aria-hidden="true"></i>
                                            <span>Class: <fieldset>asdas</fieldset>  </span>
                                        </li>
                                        <li>
                                            <i class="fa fa-certificate" aria-hidden="true"></i>
                                            <span>Subject: <fieldset>asdas</fieldset> </span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="appy-btm">
                                    <button> Apply Now</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="we_dona">
                <div class="we-cource-hw">
                    <h4>Islamic Studies </h4>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-xs-12 mt-3 m-b" data-aos="fade-up" data-aos-duration="2000">
                        <div class="beine-course-head">
                            <div class="beine-course-img">
                                <img src="asset/img/courses/courses-img-slack.05.png" width="100%" alt="">
                            </div>
                            <div class="beine-pad-head">
                                <div class="beine-course-sub">
                                    <div class="beine-course-title">
                                        <h3>Tilawathul Qur'an</h3>
                                        <p>It was popularised of Letraset Aldus PageMaker including versions of Lorem Ipsum</p>
                                    </div>
                                </div>
                                <div class="beine-course-ul">
                                    <ul>
                                        <li>
                                            <i class="fa fa-certificate" aria-hidden="true"></i>
                                            <span>Name : <fieldset>asdas</fieldset> </span>
                                        </li>
                                        <li>
                                            <i class="fa fa-certificate" aria-hidden="true"></i>
                                            <span>Class: <fieldset>asdas</fieldset>  </span>
                                        </li>
                                        <li>
                                            <i class="fa fa-certificate" aria-hidden="true"></i>
                                            <span>Subject: <fieldset>asdas</fieldset> </span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="appy-btm">
                                    <button> Apply Now</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-xs-12 mt-3 m-b" data-aos="fade-up" data-aos-duration="3000">
                        <div class="beine-course-head">
                            <div class="beine-course-img">
                                <img src="asset/img/courses/courses-img-slack.06.png" width="100%" alt="">
                            </div>
                            <div class="beine-pad-head">
                                <div class="beine-course-sub">
                                    <div class="beine-course-title">
                                        <h3>Hifz</h3>
                                        <p>It was popularised of Letraset Aldus PageMaker including versions of Lorem Ipsum</p>
                                    </div>
                                </div>
                                <div class="beine-course-ul">
                                    <ul>
                                        <li>
                                            <i class="fa fa-certificate" aria-hidden="true"></i>
                                            <span>Name : <fieldset>asdas</fieldset> </span>
                                        </li>
                                        <li>
                                            <i class="fa fa-certificate" aria-hidden="true"></i>
                                            <span>Class: <fieldset>asdas</fieldset>  </span>
                                        </li>
                                        <li>
                                            <i class="fa fa-certificate" aria-hidden="true"></i>
                                            <span>Subject: <fieldset>asdas</fieldset> </span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="appy-btm">
                                    <button> Apply Now</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-xs-12 mt-3 m-b" data-aos="fade-up" data-aos-duration="2000">
                        <div class="beine-course-head">
                            <div class="beine-course-img">
                                <img src="asset/img/courses/courses-img-slack.07.png" width="100%" alt="">
                            </div>
                            <div class="beine-pad-head">
                                <div class="beine-course-sub">
                                    <div class="beine-course-title">
                                        <h3>Mora Studies</h3>
                                        <p>It was popularised of Letraset Aldus PageMaker including versions of Lorem Ipsum</p>
                                    </div>
                                </div>
                                <div class="beine-course-ul">
                                    <ul>
                                        <li>
                                            <i class="fa fa-certificate" aria-hidden="true"></i>
                                            <span>Name : <fieldset>asdas</fieldset> </span>
                                        </li>
                                        <li>
                                            <i class="fa fa-certificate" aria-hidden="true"></i>
                                            <span>Class: <fieldset>asdas</fieldset>  </span>
                                        </li>
                                        <li>
                                            <i class="fa fa-certificate" aria-hidden="true"></i>
                                            <span>Subject: <fieldset>asdas</fieldset> </span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="appy-btm">
                                    <button> Apply Now</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="we-cource-hw">
                        <h4>Others </h4>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-3 col-xs-12 mt-3 m-b" data-aos="fade-up" data-aos-duration="2000">
                            <div class="beine-course-head">
                                <div class="beine-course-img">
                                    <img src="asset/img/courses/courses-img-slack.05.png" width="100%" alt="">
                                </div>
                                <div class="beine-pad-head">
                                    <div class="beine-course-sub">
                                        <div class="beine-course-title">
                                            <h3>IT</h3>
                                            <p>It was popularised of Letraset Aldus PageMaker including versions of Lorem Ipsum</p>
                                        </div>
                                    </div>
                                    <div class="beine-course-ul">
                                        <ul>
                                            <li>
                                                <i class="fa fa-certificate" aria-hidden="true"></i>
                                                <span>Name : <fieldset>asdas</fieldset> </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-certificate" aria-hidden="true"></i>
                                                <span>Class: <fieldset>asdas</fieldset>  </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-certificate" aria-hidden="true"></i>
                                                <span>Subject: <fieldset>asdas</fieldset> </span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="appy-btm">
                                        <button> Apply Now</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-3 col-xs-12 mt-3 m-b" data-aos="fade-up" data-aos-duration="3000">
                            <div class="beine-course-head">
                                <div class="beine-course-img">
                                    <img src="asset/img/courses/courses-img-slack.06.png" width="100%" alt="">
                                </div>
                                <div class="beine-pad-head">
                                    <div class="beine-course-sub">
                                        <div class="beine-course-title">
                                            <h3>General knowledge</h3>
                                            <p>It was popularised of Letraset Aldus PageMaker including versions of Lorem Ipsum</p>
                                        </div>
                                    </div>
                                    <div class="beine-course-ul">
                                        <ul>
                                            <li>
                                                <i class="fa fa-certificate" aria-hidden="true"></i>
                                                <span>Name : <fieldset>asdas</fieldset> </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-certificate" aria-hidden="true"></i>
                                                <span>Class: <fieldset>asdas</fieldset>  </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-certificate" aria-hidden="true"></i>
                                                <span>Subject: <fieldset>asdas</fieldset> </span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="appy-btm">
                                        <button> Apply Now</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="we-cource-hw">
                        <h4>Non Scholastic Subjects </h4>
                    </div>
                    <div class="row">


                        <div class="col-lg-6 col-md-3 col-xs-12 mt-3 m-b" data-aos="fade-up" data-aos-duration="3000">

                            <div class="beine-course-head">
                                <div class="beine-course-img">
                                    <img src="asset/img/courses/courses-img-slack.06.png" width="100%" alt="">
                                </div>
                                <div class="beine-pad-head">
                                    <div class="beine-course-sub">
                                        <div class="beine-course-title">
                                            <h3>Folklore Music</h3>
                                            <p>It was popularised of Letraset Aldus PageMaker including versions of Lorem Ipsum</p>
                                        </div>
                                    </div>
                                    <div class="beine-course-ul">
                                        <ul>
                                            <li>
                                                <i class="fa fa-certificate" aria-hidden="true"></i>
                                                <span>Name : <fieldset>asdas</fieldset> </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-certificate" aria-hidden="true"></i>
                                                <span>Class: <fieldset>asdas</fieldset>  </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-certificate" aria-hidden="true"></i>
                                                <span>Subject: <fieldset>asdas</fieldset> </span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="appy-btm">
                                        <button> Apply Now</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-3 col-xs-12 mt-3 m-b" data-aos="fade-up" data-aos-duration="3000">
                            <div class="beine-course-head">
                                <div class="beine-course-img">
                                    <img src="asset/img/courses/courses-img-slack.06.png" width="100%" alt="">
                                </div>
                                <div class="beine-pad-head">
                                    <div class="beine-course-sub">
                                        <div class="beine-course-title">
                                            <h3>Art / Craft</h3>
                                            <p>It was popularised of Letraset Aldus PageMaker including versions of Lorem Ipsum</p>
                                        </div>
                                    </div>
                                    <div class="beine-course-ul">
                                        <ul>
                                            <li>
                                                <i class="fa fa-certificate" aria-hidden="true"></i>
                                                <span>Name : <fieldset>asdas</fieldset> </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-certificate" aria-hidden="true"></i>
                                                <span>Class: <fieldset>asdas</fieldset>  </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-certificate" aria-hidden="true"></i>
                                                <span>Subject: <fieldset>asdas</fieldset> </span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="appy-btm">
                                        <button> Apply Now</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="we_dona">
                <div class="we-cource-hw">
                    <h4>Sports / Games</h4>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-xs-12 mt-3 m-b" data-aos="fade-up" data-aos-duration="2000">
                        <div class="beine-course-head">
                            <div class="beine-course-img">
                                <img src="asset/img/courses/courses-img-slack.01.png" width="100%" alt="">
                            </div>
                            <div class="beine-pad-head">
                                <div class="beine-course-sub">
                                    <div class="beine-course-title">
                                        <h3>Martial Arts</h3>
                                        <p>It was popularised of Letraset Aldus PageMaker including versions of Lorem Ipsum</p>
                                    </div>
                                </div>
                                <div class="beine-course-ul">
                                    <ul>
                                        <li>
                                            <i class="fa fa-certificate" aria-hidden="true"></i>
                                            <span>Name : <fieldset>asdas</fieldset> </span>
                                        </li>
                                        <li>
                                            <i class="fa fa-certificate" aria-hidden="true"></i>
                                            <span>Class: <fieldset>asdas</fieldset>  </span>
                                        </li>
                                        <li>
                                            <i class="fa fa-certificate" aria-hidden="true"></i>
                                            <span>Subject: <fieldset>asdas</fieldset> </span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="appy-btm">
                                    <button> Apply Now</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-xs-12 mt-3 m-b" data-aos="fade-up" data-aos-duration="3000">
                        <div class="beine-course-head">
                            <div class="beine-course-img">
                                <img src="asset/img/courses/courses-img-slack.02.png" width="100%" alt="">
                            </div>
                            <div class="beine-pad-head">
                                <div class="beine-course-sub">
                                    <div class="beine-course-title">
                                        <h3>Skating</h3>
                                        <p>It was popularised of Letraset Aldus PageMaker including versions of Lorem Ipsum</p>
                                    </div>
                                </div>
                                <div class="beine-course-ul">
                                    <ul>
                                        <li>
                                            <i class="fa fa-certificate" aria-hidden="true"></i>
                                            <span>Name : <fieldset>asdas</fieldset> </span>
                                        </li>
                                        <li>
                                            <i class="fa fa-certificate" aria-hidden="true"></i>
                                            <span>Class: <fieldset>asdas</fieldset>  </span>
                                        </li>
                                        <li>
                                            <i class="fa fa-certificate" aria-hidden="true"></i>
                                            <span>Subject: <fieldset>asdas</fieldset> </span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="appy-btm">
                                    <button> Apply Now</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-xs-12 mt-3 m-b" data-aos="fade-up" data-aos-duration="2000">
                        <div class="beine-course-head">
                            <div class="beine-course-img">
                                <img src="asset/img/courses/courses-img-slack.03.png" width="100%" alt="">
                            </div>
                            <div class="beine-pad-head">
                                <div class="beine-course-sub">
                                    <div class="beine-course-title">
                                        <h3>Foodball</h3>
                                        <p>It was popularised of Letraset Aldus PageMaker including versions of Lorem Ipsum</p>
                                    </div>
                                </div>
                                <div class="beine-course-ul">
                                    <ul>
                                        <li>
                                            <i class="fa fa-certificate" aria-hidden="true"></i>
                                            <span>Name : <fieldset>asdas</fieldset> </span>
                                        </li>
                                        <li>
                                            <i class="fa fa-certificate" aria-hidden="true"></i>
                                            <span>Class: <fieldset>asdas</fieldset>  </span>
                                        </li>
                                        <li>
                                            <i class="fa fa-certificate" aria-hidden="true"></i>
                                            <span>Subject: <fieldset>asdas</fieldset> </span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="appy-btm">
                                    <button> Apply Now</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-xs-12 mt-3 m-b" data-aos="fade-up" data-aos-duration="3000">
                        <div class="beine-course-head">
                            <div class="beine-course-img">
                                <img src="asset/img/courses/courses-img-slack.04.png" width="100%" alt="">
                            </div>
                            <div class="beine-pad-head">
                                <div class="beine-course-sub">
                                    <div class="beine-course-title">
                                        <h3>Physical Training</h3>
                                        <p>It was popularised of Letraset Aldus PageMaker including versions of Lorem Ipsum</p>
                                    </div>
                                </div>
                                <div class="beine-course-ul">
                                    <ul>
                                        <li>
                                            <i class="fa fa-certificate" aria-hidden="true"></i>
                                            <span>Name : <fieldset>asdas</fieldset> </span>
                                        </li>
                                        <li>
                                            <i class="fa fa-certificate" aria-hidden="true"></i>
                                            <span>Class: <fieldset>asdas</fieldset>  </span>
                                        </li>
                                        <li>
                                            <i class="fa fa-certificate" aria-hidden="true"></i>
                                            <span>Subject: <fieldset>asdas</fieldset> </span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="appy-btm">
                                    <button> Apply Now</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
        <div class="row">
            <div class="col-md-4 col-lg-4 col-sm-12 mt-4 mb-2 text-center">
                <div class="biene-sub">
                    <div class="biene-sub-larg">
                        <img src="asset/img/sub-images01.png" alt="">
                    </div>
                    <div class="biene-sub-titl">
                        <h3>Preschool (3 - 6 Years) </h3>
                        <i>Al Fitrah (EGYPTIAN  : Nur ul Bayan Wal Fathu Rabbani)</i>
                        <p>  Affiliated to the Egyptian curriculum, the Al Fitrah Islamic Preschool functions under BWS. Known for its innovative, novel and-</p>
                        <div class="row">
                            <div class="col-md-6 col-lg-6">
                                <div class="biene-link-b">
                                    <a href="preschool-al-fitrah.php" class="color-1">Read More</a>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="appy-btm">
                                    <button> Apply Now</button>
                                </div>
                           </div>
                        </div>
                        
                        <!-- <button class="btn-hover color-1 btnOpenForm" class="btnOpenForm">Read More</button> -->
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-12 mt-4 mb-2 text-center">
                <div class="biene-sub">
                    <div class="biene-sub-larg">
                        <img src="asset/img/sub-images02.png" alt="">
                    </div>
                    <div class="biene-sub-titl">
                        <h3>School of Hifz (6 Years and Above) </h3>
                        <i>(EGYPTIAN : Only  after completion of Al Fitrah Preschool course )</i>
                        <p>  For those who want to get educated in a spiritual environment preparing for both the worlds here is an opportunity to continue</p>
                        <div class="row">
                            <div class="col-md-6 col-lg-6">
                                <div class="biene-link-b">
                                    <a href="school-of-hifz.php" class="color-1">Read More</a>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="appy-btm">
                                    <button> Apply Now</button>
                                </div>
                           </div>
                        </div>
                        <!-- <button class="btn-hover color-1 btnOpenForm" id="btnOpenForm">Read More</button> -->
                    </div>
                </div>

            </div>
            <div class="col-md-4 col-lg-4 col-sm-12 mt-4 mb-2 text-center">
                <div class="biene-sub">
                    <div class="biene-sub-larg">
                        <img src="asset/img/sub-images03.png" alt="">
                    </div>
                    <div class="biene-sub-titl">
                        <h3>Preparatory (4 Years and Above)</h3>
                        <i>IGCSE</i>
                        <p>  Students have an experience of learning the Cambridge way of Kindergarten from Prep onwards. This one year course is </p>
                        <div class="row">
                            <div class="col-md-6 col-lg-6">
                                <div class="biene-link-b">
                                    <a href="preparatory.php" class="color-1">Read More</a>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="appy-btm">
                                    <button> Apply Now</button>
                                </div>
                           </div>
                        </div>
                        <!-- <a href="preparatory.php" class="btn-hover color-1">Read More</a> -->
                        <!-- <button class="btn-hover color-1 btnOpenForm" id="btnOpenForm">Read More</button> -->
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-12 mt-4 mb-2 text-center">
                <div class="biene-sub">
                    <div class="biene-sub-larg">
                        <img src="asset/img/sub-images05.png" alt="">
                    </div>
                    <div class="biene-sub-titl">
                        <h3>Lower and Upper Primary (Std 1 - 7)</h3>
                        <i>IGCSE</i>
                        <p>  With a reputation for providing the International curriculum at a low cost, Bienewabe World School is honoured to</p>
                        <div class="row">
                            <div class="col-md-6 col-lg-6">
                                <div class="biene-link-b">
                                    <a href="lower&upper-primary.php" class="color-1">Read More</a>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="appy-btm">
                                    <button> Apply Now</button>
                                </div>
                            </div>
                        </div>
                        <!-- <a href="lower&upper-primary.php" class="btn-hover color-1">Read More</a> -->
                        <!-- <button class="btn-hover color-1  btnOpenForm" id="btnOpenForm">Read More</button> -->
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-12 mt-4 mb-2 text-center">
                <div class="biene-sub">
                    <div class="biene-sub-larg">
                        <img src="asset/img/sub-images04.png" alt="">
                    </div>
                    <div class="biene-sub-titl">
                        <h3>High School</h3>
                        <i>SCERT</i>
                        <p>Giving focus on Medical and Engineering, students with an exposure in International curriculum can prepare for the SSLC</p>
                        <div class="row">
                            <div class="col-md-6 col-lg-6">
                                <div class="biene-link-b">
                                    <a href="high-school.php" class="color-1">Read More</a>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="appy-btm">
                                    <button> Apply Now</button>
                                </div>
                            </div>
                        </div>
                        
                        <!-- <a href="high-school.php" class="btn-hover color-1">Read More</a> -->
                        <!-- <button class="btn-hover color-1  btnOpenForm" id="btnOpenForm">Read More</button> -->
                    </div>
                </div>
                </div>
            <div class="col-md-4 col-lg-4 col-sm-12 mt-4 mb-2 text-center">
                <div class="biene-sub">
                    <div class="biene-sub-larg">
                        <img src="asset/img/sub-images06.png" alt="">
                    </div>
                    <div class="biene-sub-titl">
                        <h3>Higher Secondary</h3>
                        <i> Commerce Stream</i>
                        <p>The best investment one can make is in one's own self. The balance sheet of a student who invests in education, </p>
                        <div class="row">
                            <div class="col-md-6 col-lg-6">
                                <div class="biene-link-b">
                                    <a href="higher-secondary.php" class="color-1">Read More</a>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="appy-btm">
                                    <button> Apply Now</button>
                                </div>
                            </div>
                        </div>
                        
                        <!-- <a href="higher-secondary.php" class="btn-hover color-1">Read More</a> -->
                        <!-- <button class="btn-hover color-1  btnOpenForm" id="btnOpenForm">Read More</button> -->
                    </div>
                </div>
              </div>


         </div>
        </div>


    </section>

    <!-- Whatsapp chat  -->
    <a href="https://api.whatsapp.com/send?phone=9349157474&text=Merhaba" class="float" target="_blank">
        <i class="fa fa-whatsapp wp-button"></i>
    </a>
    <!-- Call Chat   -->
    <div class="call-us">
        <a href="tel: +919349157474">
            <img src="asset/icons/call-center.png">
        </a>
    </div>
    <?php include('links/script.php'); ?>
    <?php include('links/footer.php'); ?>
    <script>
        AOS.init();
    </script>
</body>

</html>