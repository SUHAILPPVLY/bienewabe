<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Bienewabe | World School | An Initative of The B School International</title>
    <meta name=description content="Bienewabe World School An Initative of The B School International, Kottakkal">
    <meta name="keywords" content="Bienewabe World School An Initative of The B School International, Kottakkal">
    <?php include('links/style.php'); ?>
</head>

<body>
    <!-- Header area  -->
    <?php include('links/header.php'); ?>
    <!-- slider area  -->
    <?php include('links/slider.php'); ?>
    <!-- top button  -->

    <!-- Return to Top -->
    <a href="javascript:" id="return-to-top">
        <img src="asset/icons/top-arrow.png" alt="">
    </a>
    <!-- Return to Top -->
    <!-- Vedio Head  -->
    <!-- <div class="biene-wide-br">
        <button id="popup-btn-vedio" class="brochure-side">Bienewabe @ 
            <img src="asset/icons/bienewabe-vedio-sb.png" alt=""> </button>
    </div>
    <div class="video-popup">
        <div class="popup-bg"></div>
        <div class="popup-content_as">
            <iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2F1547237498887957%2Fvideos%2F2044374392507596&width=500&show_text=false&height=250&appId" class="video" style="border:none;overflow:hidden" scrolling="no" frameborder="0"
                allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share" allowFullScreen="true"></iframe>
            <button class="close-btn beine-close">X</button>
        </div>
    </div> -->
    <div class="biene-wide-br">
        <div class="brochure-side" id="popup-btn-vedio">
            <a class="popup-youtube" href="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2F1547237498887957%2Fvideos%2F2044374392507596&width=500&show_text=false&height=250&appId">Bienewabe @ <img src="asset/icons/bienewabe-vedio-sb.png" alt=""></a>
        </div>
    </div>
    <!-- Vedio Head end  -->

    <!-- Icon part -->
    <section class="section-1">
        <div class="container-fluid">
            <div class="col-md-12 col-lg-12 text-center">
                <div class="biene-team">
                    <h2>Our <span>Syllabus</span></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <div class="row">
                        <div class="col-md-4 col-lg-4 col-sm-12 mb-2 mt-2 bienewabe-bord bienewabe-bord-none" data-aos="fade-up" data-aos-duration="2000">
                            <div class="bienewabe-branch">
                                <div class="bienewabe-branch-top">
                                    <img class="been-img" src="asset/img/biene-war-school-img.png" alt="">
                                </div>

                                <div class="bienewabe-branch-centre">
                                    <h2>Kerala Syllabus [5th - 10th] </h2>
                                    <p>The State Council for Educational Research and Training (SCERT) Kerala is a board of school education in India, conducted by the Government of Kerala, also prepares the syllabus for schools. The main academic focus
                                        here is on Medical & Engineering. Ad added advantage is that we make sure that the students of our national curriculum will also be reflecting the International exposure with the help of our existing teachers who
                                        are well expertise with the global educational system.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8 col-lg-8 col-sm-12 mb-2 mt-2 bienewabe-bord bienewabe-bord-none" data-aos="fade-down" data-aos-duration="2000">
                            <div class="bienewabe-branch">
                                <div class="bienewabe-branch-top">
                                    <img class="been-img" src="asset/img/biene-war-school-meeting-img.png" alt="">
                                </div>
                                <div class="bienewabe-branch-centre">
                                    <h2>General Info</h2>
                                    <ul class="bienewabe-ul-awa">
                                        <li>
                                            <h3><img class="bien-imgs" src="asset/icons/ict-lab.png" alt=""></i>ICT Labs</h3>
                                            <p>Bienewabe has got a well equipped Informi Communications Technology Lab set up to ensure that our students are always kept updated with the latest technology.</p>
                                        </li>
                                        <li>
                                            <h3><img class="bien-imgs" src="asset/icons/transport-vehicle.png" alt=""></i>Transport Facilities</h3>
                                            <p>A contract is been signed with an external logistics agent which covers around 1o kms radius of the school.</p>
                                        </li>
                                        <li>
                                            <h3><img class="bien-imgs" src="asset/icons/uniformity.png" alt=""> Uniforms</h3>
                                            <p>We at Bienewabe World School favours uniform codes because they lead to a safer educational environment that increases students ability to learn and increases equality between them. The Uniform code of the school
                                                includes the Clothing, Shoe and Bag wil be available at Bienewabe store.</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-12 mb-2 mt-2 " data-aos="fade-up" data-aos-duration="2000">
                    <div class="bienewabe-branch">
                        <div class="bienewabe-branch-top">
                            <img class="been-img" src="asset/img/biene-war-school-class-img.png" alt="">
                        </div>
                        <div class="bienewabe-branch-centre">
                            <h2>Higher Secondary [+1/+2]</h2>
                            <p>Students who pass out from the SSLC/IGCSE are offered both +2 Kerala Stream as well as the IGCSEA levels. Science and commerce streamsare offered in the Higher Secondary levels. Extra curricular activities and other research
                                based practical learning inputs are given to all our children alike so as to fulfill our motto " Learn through Experiences & Experiments"</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Icon part end-->

    <!-- About Section  -->
    <section class="bienewabe_abt_bg">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-lg-6">
                    <div class="biene-dots-img">
                        <img src="asset/img/about-bees-fly.png" width="100%" alt="">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-lg-6">
                    <div class="bien-abouts">
                        <div class="biene-margs">
                            <h2 class="" data-aos="fade-up" data-aos-duration="1000">About us</h2>
                            <p class="" data-aos="fade-up" data-aos-duration="2000">Driven by the motto to let pupil Learn through experiences & experiments' we aim to promote a research based, practical learning system implem- ented through rigorous guidance. This will further promote learners think critically,
                                synthesize knowledge, reflect own thought-process and thus transform the coming generations into global citizens of high values. They will be embodying as agents of those who make common sense into common practice.
                            </p>
                            <p class="" data-aos="fade-up" data-aos-duration="3000">
                                Our Philosophy is that knowledge is the most important thing in one's life not only beneficial for this day but the here-after as well, hence we are motivated to push the right knowledge into them the right way so as to groom them become citizens off
                                the society with a global vision.
                            </p>
                            <a class="btn-hover color-1" data-aos="fade-up" data-aos-duration="3000" href="aboutus.php">Read More</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <!-- About part end-->

    <section class="section-1">
        <div class="container">
            <div class="col-md-12 col-lg-12 text-center">
                <div class="biene-team">
                    <h2>Under its wings, <span>BWS has the following sections</span> </h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div id="customers-testimonials-wing" class="owl-carousel">
                        <div class="item item-bg mt-4 mb-2 text-center" data-aos="fade-up" data-aos-duration="2000">
                            <div class="biene-sub">
                                <div class="biene-sub-larg">
                                    <img src="asset/img/sub-images01.png" alt="">
                                </div>
                                <div class="biene-sub-titl">
                                    <h3>Preschool (3 - 6 Years) </h3>
                                    <i>Al Fitrah (EGYPTIAN  : Nur ul Bayan Wal Fathu Rabbani)</i>
                                    <p>Affiliated to the Egyptian curriculum, the Al Fitrah Islamic Preschool functions under BWS.</p>
                                    <div class="row">
                                        <div class="col-md-6 col-lg-6">
                                            <div class="biene-link-b">
                                                <a href="preschool-al-fitrah.php" class="color-1">Read More</a>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-6">
                                            <div class="appy-btm">
                                                <button> Apply Now</button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <a href="preschool-al-fitrah.php" class="btn-hover color-1">Read More</a> -->
                                    <!-- <button class="btn-hover color-1 btnOpenForm" class="btnOpenForm">Read More</button> -->
                                </div>
                            </div>
                        </div>

                        <div class="item item-bg mt-4 mb-2 text-center" data-aos="fade-up" data-aos-duration="3000">
                            <div class="biene-sub">
                                <div class="biene-sub-larg">
                                    <img src="asset/img/sub-images02.png" alt="">
                                </div>
                                <div class="biene-sub-titl">
                                    <h3>School of Hifz (6 Years and Above) </h3>
                                    <i>(EGYPTIAN : Only  after completion of Al Fitrah Preschool course )</i>
                                    <p>For those who want to get educated in a spiritual environment preparing for both the</p>
                                    <div class="row">
                                        <div class="col-md-6 col-lg-6">
                                            <div class="biene-link-b">
                                                <a href="school-of-hifz.php" class="color-1">Read More</a>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-6">
                                            <div class="appy-btm">
                                                <button> Apply Now</button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <a href="school-of-hifz.php" class="btn-hover color-1">Read More</a> -->
                                    <!-- <button class="btn-hover color-1 btnOpenForm" id="btnOpenForm">Read More</button> -->
                                </div>
                            </div>

                        </div>

                        <div class="item item-bg mt-4 mb-2 text-center" data-aos="fade-up" data-aos-duration="2000">
                            <div class="biene-sub">
                                <div class="biene-sub-larg">
                                    <img src="asset/img/sub-images03.png" alt="">
                                </div>
                                <div class="biene-sub-titl">
                                    <h3>Preparatory (4 Years and Above)</h3>
                                    <i>IGCSE</i>
                                    <p>Students have an experience of learning the Cambridge way of Kindergarten from-</p>
                                    <div class="row">
                                        <div class="col-md-6 col-lg-6">
                                            <div class="biene-link-b">
                                                <a href="preparatory.php" class="color-1">Read More</a>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-6">
                                            <div class="appy-btm">
                                                <button> Apply Now</button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <a href="preparatory.php" class="btn-hover color-1">Read More</a> -->
                                    <!-- <button class="btn-hover color-1 btnOpenForm" id="btnOpenForm">Read More</button> -->
                                </div>
                            </div>

                        </div>

                        <div class="item item-bg mt-4 mb-2 text-center" data-aos="fade-up" data-aos-duration="3000">
                            <div class="biene-sub">
                                <div class="biene-sub-larg">
                                    <img src="asset/img/sub-images05.png" alt="">
                                </div>
                                <div class="biene-sub-titl">
                                    <h3>Lower and Upper Primary (Std 1 - 7)</h3>
                                    <i>IGCSE</i>
                                    <p>With a reputation for providing the International curriculum at a low cost,</p>
                                    <div class="row">
                                        <div class="col-md-6 col-lg-6">
                                            <div class="biene-link-b">
                                                <a href="lower&upper-primary.php" class="color-1">Read More</a>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-6">
                                            <div class="appy-btm">
                                                <button> Apply Now</button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <a href="lower&upper-primary.php" class="btn-hover color-1">Read More</a> -->
                                    <!-- <button class="btn-hover color-1  btnOpenForm" id="btnOpenForm">Read More</button> -->
                                </div>
                            </div>
                        </div>
                        <div class="item item-bg mt-4 mb-2 text-center" data-aos="fade-up" data-aos-duration="2000">
                            <div class="biene-sub">
                                <div class="biene-sub-larg">
                                    <img src="asset/img/sub-images04.png" alt="">
                                </div>
                                <div class="biene-sub-titl">
                                    <h3>High School</h3>
                                    <i>SCERT</i>
                                    <p>Giving focus on Medical and Engineering, students with an exposure in International</p>
                                    <div class="row">
                                        <div class="col-md-6 col-lg-6">
                                            <div class="biene-link-b">
                                                <a href="high-school.php" class="color-1">Read More</a>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-6">
                                            <div class="appy-btm">
                                                <button> Apply Now</button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <a href="high-school.php" class="btn-hover color-1">Read More</a> -->
                                    <!-- <button class="btn-hover color-1  btnOpenForm" id="btnOpenForm">Read More</button> -->
                                </div>
                            </div>
                        </div>
                        <div class="item item-bg mt-4 mb-2 text-center" data-aos="fade-up" data-aos-duration="2000">
                            <div class="biene-sub">
                                <div class="biene-sub-larg">
                                    <img src="asset/img/sub-images06.png" alt="">
                                </div>
                                <div class="biene-sub-titl">
                                    <h3>Higher Secondary</h3>
                                    <i> Commerce Stream</i>
                                    <p>The best investment one can make is in one's own self. The balance sheet of a student</p>
                                    <div class="row">
                                        <div class="col-md-6 col-lg-6">
                                            <div class="biene-link-b">
                                                <a href="higher-secondary.php" class="color-1">Read More</a>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-6">
                                            <div class="appy-btm">
                                                <button> Apply Now</button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <a href="higher-secondary.php" class="btn-hover color-1">Read More</a> -->
                                    <!-- <button class="btn-hover color-1  btnOpenForm" id="btnOpenForm">Read More</button> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-12 text-center">
                        <a href="we-cources.php" class="btn-hover color-1 home-btn">View all</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Directors Messsage  -->
    <!-- Fixed image  -->
    <section class="beine-test">
        <div class="container">
            <div class="col-md-12 col-lg-12">
                <div class="biein-call">
                    <h3><span>Get Ready </span> to Experience, Experiment and Explore...!</h3>
                </div>
            </div>
        </div>
    </section>
    <!-- Message From Directors  -->
    <section class="section-1">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-lg-8 col-xs-12 mt-2 mb-2">
                    <div class="biene-for-message">
                        <h3>Message from the Directors</h3>
                        <p data-aos="fade-up" data-aos-duration="1000">Since its inception, our sole aim had been to provide quality education in International Curriculum to the students of Malabar. Over the short span, we have set new benchmarks in Global Education by the excellent track records
                            at our first international graduate school called 'The B School International. We strive to enable our young minds who have the intelligence, talent and potential to become the most successtul pupil around the globe and to
                            grabemerging opportunities offered by the globalization.</p>
                        <p data-aos="fade-up" data-aos-duration="2000">Accordingly, this particular initiative of offering an International education starting from pre-school levels, came up as a solution to bridge the gap between the 'by-heart' method of typical education with that of the International
                            curriculum in UG/PG levels; as we found that our graduate students at The B School International struggle to win the battle with the highly designed and structured global syllabus due to their ready-made schooling background.
                            This has been a pain for us over the years resulting an eye- opening towards an initiative to avail our children's chances to experience the International syllabus from sch0ol level, so as to equip them ready to pursue their
                            higher studies in renowned Universities around the globe.
                        </p>
                        <p data-aos="fade-up" data-aos-duration="3000">
                            We make sure that our students possess the necessary know-how so as to spruce up our future professionals, sharpen their ability, actualize their potential and also that every care is taken that they become successful professionals of high ethical values
                            and responsible members of the society.
                        </p>
                        <!-- <a href="#" class="btn-hover color-1" data-aos="fade-up" data-aos-duration="4000">Read More</a> -->
                    </div>
                </div>
                <div class="col-sm-12 col-lg-4 col-xs-12 mt-2 mb-2" data-aos="fade-in" data-aos-duration="2000">
                    <div class="biene-for-message-img">
                        <img src="asset/img/bienewabe-holder.png" width="100%" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Directors Messsage end  -->
    <!-- TESTIMONIALS -->
    <section class="testimonials biene-fixed-dm">
        <div class="container">
            <div class="col-md-12 col-lg-12 text-center">
                <div class="biene-test">
                    <h2>Our Videos</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div id="customers-testimonials" class="owl-carousel">
                        <!--TESTIMONIAL 1 -->
                        <div class="item item-bg mt-2 mb-2 mr-2" data-aos="fade-up" data-aos-duration="2000">
                            <iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fwatch%2F%3Fv%3D419530385465337&width=500&show_text=false&height=330&appId" width="100%" height="330px" style="border:none;overflow:hidden" scrolling="no" frameborder="0"
                                allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share" allowFullScreen="true"></iframe>
                            <!-- <div class="shadow-effect">
                                <img class="img-responsive" src="asset/img/our-ban-fix_images01.jpg" width="100%" alt="">
                                <div class="item-heads">
                                    <div class="item-details">
                                        <h5>Lorem Ipsum</h5>
                                        <p> Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. </p>
                                    </div>
                                    <div class="item-fas text-center">
                                        <span>Chief Editor</span>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                        <!--END OF TESTIMONIAL 1 -->
                        <!--TESTIMONIAL 2 -->
                        <div class="item item-bg mt-2 mb-2 mr-2" data-aos="fade-up" data-aos-duration="3000">
                            <iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2F1547237498887957%2Fvideos%2F1548450098766697&width=500&show_text=false&height=330&appId" width="100%" height="330px" style="border:none;overflow:hidden" scrolling="no"
                                frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share" allowFullScreen="true"></iframe>

                            <!-- <div class="shadow-effect">
                                <img class="img-responsive" src="asset/img/our-ban-fix_images02.jpg" alt="">
                                <div class="item-heads">
                                    <div class="item-details">
                                        <h5>Lorem Ipsum</h5>
                                        <p> Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. </p>
                                    </div>
                                    <div class="item-fas text-center">
                                        <span>Chief Editor</span>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                        <!--END OF TESTIMONIAL 2 -->
                        <!--TESTIMONIAL 3 -->
                        <div class="item item-bg mt-2 mb-2 mr-2" data-aos="fade-up" data-aos-duration="2000">
                            <iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2F1547237498887957%2Fvideos%2F2793765844014228&width=500&show_text=false&height=330&appId" width="100%" height="330px" style="border:none;overflow:hidden" scrolling="no"
                                frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share" allowFullScreen="true"></iframe>

                            <!-- <div class="shadow-effect">
                                <img class="img-responsive" src="asset/img/our-ban-fix_images03.jpg" alt="">
                                <div class="item-heads">
                                    <div class="item-details">
                                        <h5>Lorem Ipsum</h5>
                                        <p> Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. </p>
                                    </div>
                                    <div class="item-fas text-center">
                                        <span>Chief Editor</span>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                        <!--END OF TESTIMONIAL 3 -->
                        <!--TESTIMONIAL 4 -->
                        <div class="item item-bg mt-2 mb-2 mr-2" data-aos="fade-up" data-aos-duration="3000">
                            <iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2F1547237498887957%2Fvideos%2F333104241056356&width=500&show_text=false&height=282&appId" width="100%" height="330px" style="border:none;overflow:hidden" scrolling="no"
                                frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share" allowFullScreen="true"></iframe>

                            <!-- <div class="shadow-effect">
                                <img class="img-responsive" src="asset/img/our-ban-fix_images04.jpg" alt="">
                                <div class="item-heads">
                                    <div class="item-details">
                                        <h5>Lorem Ipsum</h5>
                                        <p> Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. </p>
                                    </div>
                                    <div class="item-fas text-center">
                                        <span>Chief Editor</span>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                        <!--END OF TESTIMONIAL 4 -->
                        <!--TESTIMONIAL 5 -->
                        <div class="item item-bg mt-2 mb-2 mr-2" data-aos="fade-up" data-aos-duration="2000">
                            <!-- <iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fwatch%2F%3Fv%3D419530385465337&width=500&show_text=false&height=330&appId" width="100%" height="330px" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share" allowFullScreen="true"></iframe> -->

                            <!-- <div class="shadow-effect">
                                <img class="img-responsive" src="asset/img/our-ban-fix_images05.jpg" alt="">
                                <div class="item-heads">
                                    <div class="item-details">
                                        <h5>Lorem Ipsum</h5>
                                        <p> Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. </p>
                                    </div>
                                    <div class="item-fas text-center">
                                        <span>Chief Editor</span>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                        <!--END OF TESTIMONIAL 5 -->
                    </div>
                </div>
                <!-- <div class="col-lg-3 col-md-3" data-aos="fade-in" data-aos-duration="2000">
                    <div class="biene-cave">
                        <img src="asset/img/bienewabe-actul.png" width="100%" alt="">
                    </div>
                </div> -->
            </div>
        </div>
    </section>
    <!-- END OF TESTIMONIALS -->

    <!-- Gallery Section -->
    <section class="section-1 beien-dot testimonials">
        <div class="container-fluid">
            <div class="col-md-12 col-lg-12 text-center">
                <div class="biene-head">
                    <h2>Gallery</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-lg-4" data-aos="fade-out" data-aos-duration="2000">
                    <div class="biene_imgs_none">
                        <img src="asset/img/biene-image-for-side.png" width="100%" alt="">
                    </div>
                </div>

                <div class="col-md-8 col-lg-8">
                    <div class="wrapp">
                        <div class="photobanner">
                            <div class="beine-port" data-aos="fade-in" data-aos-duration="2000">
                                <img class="first" src="asset/img/bienewabe-gallery-img-01.jpg" alt="" />
                            </div>
                            <div class="beine-port" data-aos="fade-in" data-aos-duration="3000">
                                <img src="asset/img/bienewabe-gallery-img-02.jpg" alt="" />
                            </div>
                            <div class="beine-port" data-aos="fade-in" data-aos-duration="2000">
                                <img src="asset/img/bienewabe-gallery-img-03.jpg" alt="" />
                            </div>
                            <div class="beine-port" data-aos="fade-in" data-aos-duration="3000">
                                <img src="asset/img/bienewabe-gallery-img-04.jpg" alt="" />
                            </div>
                            <div class="beine-port" data-aos="fade-in" data-aos-duration="2000">
                                <img src="asset/img/bienewabe-gallery-img-05.jpg" alt="" />
                            </div>
                            <div class="beine-port" data-aos="fade-in" data-aos-duration="3000">
                                <img src="asset/img/bienewabe-gallery-img-06.jpg" alt="" />
                            </div>
                            <div class="beine-port" data-aos="fade-in" data-aos-duration="2000">
                                <img src="asset/img/bienewabe-gallery-img-07.jpg" alt="" />
                            </div>
                            <div class="beine-port" data-aos="fade-in" data-aos-duration="3000">
                                <img src="asset/img/bienewabe-gallery-img-08.jpg" alt="" />
                            </div>
                            <div class="beine-port" data-aos="fade-in" data-aos-duration="2000">
                                <img src="asset/img/bienewabe-gallery-img-09.jpg" alt="" />
                            </div>
                            <div class="beine-port" data-aos="fade-in" data-aos-duration="3000">
                                <img src="asset/img/bienewabe-gallery-img-10.jpg" alt="" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-12 text-center">
                        <a href="gallery.php" class="btn-hover color-1">View all</a>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <!-- Our Icon css  -->
    <!-- <section class="section-1">
        <div class="container-fluid">
            <div class="col-md-12 col-lg-12 text-center">
                <div class="biene-team">
                    <h2>Our Featured</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-xs-12 mb-2 mt-2" data-aos="fade-up" data-aos-duration="2000">
                    <div class="beiene-toper">
                        <div class="beiene-sups-icon">
                            <div class="beiene-sups-awa">
                                <img src="asset/icons/bienewabe-students-icon-01.png" alt="">
                            </div>
                        </div>
                        <div class="beiene-sups-huma">
                            <div class="beine-awaw-call">
                                <h2>Lorem ipsum</h2>
                                <p>Lorem ipsum dolor sit amet, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-12 mb-2 mt-2" data-aos="fade-up" data-aos-duration="3000">
                    <div class="beiene-toper">
                        <div class="beiene-sups-icon">
                            <div class="beiene-sups-awa">
                                <img src="asset/icons/bienewabe-students-icon-01.png" alt="">
                            </div>
                        </div>
                        <div class="beiene-sups-huma">
                            <div class="beine-awaw-call">
                                <h2>Lorem ipsum</h2>
                                <p>Lorem ipsum dolor sit amet, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-12 mb-2 mt-2" data-aos="fade-up" data-aos-duration="2000">
                    <div class="beiene-toper">
                        <div class="beiene-sups-icon">
                            <div class="beiene-sups-awa">
                                <img src="asset/icons/bienewabe-students-icon-01.png" alt="">
                            </div>
                        </div>
                        <div class="beiene-sups-huma">
                            <div class="beine-awaw-call">
                                <h2>Lorem ipsum</h2>
                                <p>Lorem ipsum dolor sit amet, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-12 mb-2 mt-2" data-aos="fade-up" data-aos-duration="3000">
                    <div class="beiene-toper">
                        <div class="beiene-sups-icon">
                            <div class="beiene-sups-awa">
                                <img src="asset/icons/bienewabe-students-icon-01.png" alt="">
                            </div>
                        </div>
                        <div class="beiene-sups-huma">
                            <div class="beine-awaw-call">
                                <h2>Lorem ipsum</h2>
                                <p>Lorem ipsum dolor sit amet, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-12 mb-2 mt-2" data-aos="fade-up" data-aos-duration="2000">
                    <div class="beiene-toper">
                        <div class="beiene-sups-icon">
                            <div class="beiene-sups-awa">
                                <img src="asset/icons/bienewabe-students-icon-01.png" alt="">
                            </div>
                        </div>
                        <div class="beiene-sups-huma">
                            <div class="beine-awaw-call">
                                <h2>Lorem ipsum</h2>
                                <p>Lorem ipsum dolor sit amet, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-12 mb-2 mt-2" data-aos="fade-up" data-aos-duration="3000">
                    <div class="beiene-toper">
                        <div class="beiene-sups-icon">
                            <div class="beiene-sups-awa">
                                <img src="asset/icons/bienewabe-students-icon-01.png" alt="">
                            </div>
                        </div>
                        <div class="beiene-sups-huma">
                            <div class="beine-awaw-call">
                                <h2>Lorem ipsum</h2>
                                <p>Lorem ipsum dolor sit amet, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section> -->
    <!-- Our Icon css  -->

    <!-- Whatsapp chat  -->
    <a href="https://api.whatsapp.com/send?phone=9349157474&text=Merhaba" class="float" target="_blank">
        <i class="fa fa-whatsapp wp-button"></i>
    </a>
    <!-- Call Chat   -->
    <div class="call-us">
        <a href="tel: +919349157474">
            <img src="asset/icons/call-center.png">
        </a>
    </div>

    <?php include('links/script.php'); ?>
    <?php include('links/footer.php'); ?>
    <script>
        AOS.init();
    </script>

</body>

</html>