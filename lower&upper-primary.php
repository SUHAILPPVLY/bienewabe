<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Bienewabe | World School | An Initative of The B School International</title>
    <meta name=description content="Bienewabe World School An Initative of The B School International, Kottakkal">
    <meta name="keywords" content="Bienewabe World School An Initative of The B School International, Kottakkal">
    <?php include('links/style.php'); ?>
</head>

<body>
    <!-- header area  -->
    <?php include('links/header.php'); ?>
    <!-- Return to Top -->
    <a href="javascript:" id="return-to-top">
        <img src="asset/icons/top-arrow.png" alt="">
    </a>
    <!-- Return to Top -->

    <!-- contact Banner image  -->
    <section class="bienewabe-lower-upper">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-left">
                    <div class="bien_titl">
                        <h2>Lower & Upper Primary </h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- contact Banner image  -->


    <section class="section-50 bienwall-dots">
        <div class="container">
            <div class="col-md-12 col-lg-12 text-center wing-botom">
                <div class="animate one">
                    <span>L</span><span>o</span><span>w</span><span>e</span><span>r</span>
                    <span>U</span><span>p</span><span>p</span><span>e</span><span>r</span>
                    <span>P</span><span>r</span><span>i</span><span>m</span><span>a</span><span>r</span><span>y</span>&nbsp;
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" data-aos="fade-up" data-aos-duration="2000">
                    <div class="wing-headline">
                        <h3>Lower & Upper Primary, Education for both the worlds</h3>
                        <p>With a Reputation for providing the International curriculum at a low cost, Bienewabe World School is honoured to announce that students can have access to quality education in the heart of Malabar under the umbrella of The B School
                            with eminent personalities having a background of proven excellence in academics and practice at the helm with the sole aim to impart specialized education of International standards. The key role of BWS is in teaching a combination
                            of skills giving equal weightage to moral and value education.A competent Bienewabian who has enrolled in the Preschool and continues his education from the primary level will be able to memorize at least 5 juz of the Holy
                            Qur'an before entering High School.</p>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Intention Behind  -->
    <section class="padding-bottom">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-xs-6 col-lg-6 mt-3 mb-2">
                    <div class="our-class-section">
                        <h3>Subjects offered in IGCSE Curriculum</h3>
                        <ul>
                            <li>
                                <span>
                                    <i class="fa fa-book" aria-hidden="true"></i>
                                    <p>English</p>
                                </span>
                            </li>
                            <li>
                                <span>
                                    <i class="fa fa-book" aria-hidden="true"></i>
                                    <p>Mathematics</p>
                                </span>
                            </li>
                            <li>
                                <span>
                                    <i class="fa fa-book" aria-hidden="true"></i>
                                    <p>Science</p>
                                </span>
                            </li>
                            <li class="our-class-section-none">
                                <span>
                                    <i class="fa fa-book" aria-hidden="true"></i>
                                    <p>Social Studies </p>
                                </span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-6 col-lg-6 mt-3 mb-2">
                    <div class="our-class-section">
                        <h3>Sports / Games</h3>
                        <ul>
                            <li>
                                <span>
                                    <i class="fa fa-book" aria-hidden="true"></i>
                                    <p>Martial Arts</p>
                                </span>
                            </li>
                            <li>
                                <span>
                                    <i class="fa fa-book" aria-hidden="true"></i>
                                    <p>Skating</p>
                                </span>
                            </li>
                            <li>
                                <span>
                                    <i class="fa fa-book" aria-hidden="true"></i>
                                    <p>Football</p>
                                </span>
                            </li>
                            <li class="our-class-section-none">
                                <span>
                                    <i class="fa fa-book" aria-hidden="true"></i>
                                    <p>Physical Training </p>
                                </span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-6 col-lg-6 mt-3 mb-2">
                    <div class="our-class-section">
                        <h3>Languages </h3>
                        <ul>
                            <li>
                                <span>
                                    <i class="fa fa-book" aria-hidden="true"></i>
                                    <p>Arabic</p>
                                </span>
                            </li>
                            <li>
                                <span>
                                    <i class="fa fa-book" aria-hidden="true"></i>
                                    <p>Malayalam</p>
                                </span>
                            </li>
                            <li class="our-class-section-none">
                                <span>
                                    <i class="fa fa-book" aria-hidden="true"></i>
                                    <p>Hindi</p>
                                </span>
                            </li>

                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-6 col-lg-6 mt-3 mb-2">
                    <div class="our-class-section">
                        <h3>Islamic Studies</h3>
                        <ul>
                            <li>
                                <span>
                                    <i class="fa fa-book" aria-hidden="true"></i>
                                    <p>Tilawatul Qur'an </p>
                                </span>
                            </li>
                            <li>
                                <span>
                                    <i class="fa fa-book" aria-hidden="true"></i>
                                    <p>Hifz</p>
                                </span>
                            </li>
                            <li class="our-class-section-none">
                                <span>
                                    <i class="fa fa-book" aria-hidden="true"></i>
                                    <p>Mora Studies </p>
                                </span>
                            </li>

                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-6 col-lg-6 mt-3 mb-2">
                    <div class="our-class-section">
                        <h3>Others</h3>
                        <ul>
                            <li>
                                <span>
                                    <i class="fa fa-book" aria-hidden="true"></i>
                                    <p>Information Technology </p>
                                </span>
                            </li>
                            <li class="our-class-section-none">
                                <span>
                                    <i class="fa fa-book" aria-hidden="true"></i>
                                    <p>General Knowledge </p>
                                </span>
                            </li>

                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-6 col-lg-6 mt-3 mb-2">
                    <div class="our-class-section">
                        <h3>Non Scholastic Subjects</h3>
                        <ul>
                            <li>
                                <span>
                                    <i class="fa fa-book" aria-hidden="true"></i>
                                    <p>Folklore Music</p>
                                </span>
                            </li>
                            <li class="our-class-section-none">
                                <span>
                                    <i class="fa fa-book" aria-hidden="true"></i>
                                    <p>Art / Craft</p>
                                </span>
                            </li>

                        </ul>
                    </div>
                </div>
                <!-- <div class="col-lg-12 col-lg-12 text-center">
                    <div class="wing-back-done">
                        <a href="index.php">Back to Home</a>
                    </div>
                </div> -->
                <div class="col-lg-12 col-md-12 mt-4 text-center">
                    <div class="our_textdisl">
                        <div class="wing-back-done">
                            <a href="index.php">Back to Home</a>
                        </div>
                        <div class="appy-btm">
                            <button> Apply Now</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Whatsapp chat  -->
    <a href="https://api.whatsapp.com/send?phone=9349157474&text=Merhaba" class="float" target="_blank">
        <i class="fa fa-whatsapp wp-button"></i>
    </a>
    <!-- Call Chat   -->
    <div class="call-us">
        <a href="tel: +919349157474">
            <img src="asset/icons/call-center.png">
        </a>
    </div>
    <?php include('links/script.php'); ?>
    <?php include('links/footer.php'); ?>
    <script>
        AOS.init();
    </script>
</body>

</html>