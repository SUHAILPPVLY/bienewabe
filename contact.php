<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Bienewabe | World School | An Initative of The B School International</title>
    <meta name=description content="Bienewabe World School An Initative of The B School International, Kottakkal">
    <meta name="keywords" content="Bienewabe World School An Initative of The B School International, Kottakkal">
    <?php include('links/style.php'); ?>
</head>

<body>
    <!-- header area  -->
    <?php include('links/header.php'); ?>
    <!-- Return to Top -->
    <a href="javascript:" id="return-to-top">
        <img src="asset/icons/top-arrow.png" alt="">
    </a>
    <!-- Return to Top -->

    <!-- contact Banner image  -->
    <section class="bienewabe-contact">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-left">
                    <div class="bien_titl">
                        <h2>Contact <span>us</span></h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- contact Banner image  -->

    <!-- contact us  -->
    <section class="section-50">
        <div class="container">
            <div class="col-md-12 col-lg-12 text-center">
                <div class="animate one">
                    <span>C</span><span>o</span><span>n</span><span>t</span><span>a</span><span>c</span><span>t</span>&nbsp;
                    <span>u</span><span>s</span>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12  mt-3 wow fadeInDown" data-wow-delay="0.3s" data-wow-duration="2s">
                    <div class="bienewabe-cont">
                        <div class="row">
                            <div class="col-md-4 col-lg-4 col-sm-6 text-center">
                                <div class="bien-conts">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                    <h3>Address</h3>
                                    <p> Al Manar Campus, Sirhind, Kottakkal<br> Pin: 676503, Malappuram.</p>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4 col-sm-6 text-center">
                                <div class="bien-conts">
                                    <i class="fa fa-phone" aria-hidden="true"></i>
                                    <h3>Phone</h3>
                                    <a href="tel:+91 9349 157 474">+91 9349 157 474</a><br>
                                    <a href="tel:+91 9349 157 474">+91 9349 157 474</a>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4 col-sm-6 text-center">
                                <div class="bien-conts">
                                    <i class="fa fa-envelope" aria-hidden="true"></i>
                                    <h3>Mail</h3>
                                    <a href="mailto:bienewabe.kkl@gmail.com">bienewabe.kkl@gmail.com</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <!-- google map  -->
    <section>
        <div class="container-fluid p-0">
            <div class="map-contact">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3916.8869909169553!2d76.00387621480249!3d10.971901892189019!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ba7b4883ec2af61%3A0xafeb541a3f7dd7fb!2sBienewabe%20World%20School!5e0!3m2!1sen!2sin!4v1613365053709!5m2!1sen!2sin"
                    width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div>
        </div>
    </section>
    <!-- google map  -->
    <!-- Form data  -->
    <section class="">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 being_forms wow  fadeInDown" data-wow-delay="0.3s" data-wow-duration="2s">
                    <div class="being_forms_a being_bg">
                        <div class="beig-hw">
                            <h3>Get In Touch</h3>
                            <p>In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content</p>
                        </div>
                        <form id="contact" action="form" method="post">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 ">
                                    <div class="input-beig">
                                        <input placeholder="Name*" type="text" required>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="input-beig">
                                        <input placeholder="Email Address*" type="email" tabindex="2" required>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="input-beig">
                                        <input placeholder="Phone Number" type="tel" tabindex="3" required>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="input-beig">
                                        <input placeholder="Subject (optional)" type="url" tabindex="4" required>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 ">
                                    <div class="input-beig-area">
                                        <textarea rows="4" cols="50" placeholder="Comment here..." required></textarea>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 text-center ">
                                    <div class="bt-aba">
                                        <a class="btn-hover color-1" href="#">Sent Message</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <!-- Whatsapp chat  -->
    <a href="https://api.whatsapp.com/send?phone=9349157474&text=Merhaba" class="float" target="_blank">
        <i class="fa fa-whatsapp wp-button"></i>
    </a>
    <!-- Call Chat   -->
    <div class="call-us">
        <a href="tel: +919349157474">
            <img src="asset/icons/call-center.png">
        </a>
    </div>
    <?php include('links/script.php'); ?>
    <?php include('links/footer.php'); ?>
    <script>
        AOS.init();
    </script>


</body>

</html>